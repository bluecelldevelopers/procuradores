<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">NOTICIAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<div class="module-news">
				<div class="single-news">
					<span class="news-title">Catalá condecora a 9 procuradores con la Orden de San Raimundo de Peñafort</span>
					<div class="destacado-vid">
						<div class="image-news">
							<img src="img/content/content-02.jpg" alt="">
							<span class="destacado">DESTACADO</span>
							<span class="pie">Rafael Catalá Polo, Ministro de Justicia.</span>
						</div>
						<div class="fecha-cont">
							<span class="fecha">27/07/2016</span>
						</div>
						<span class="tags">Premios, Universidad, Procuradores</span>

					</div>
					<div class="text-content">
						<p>El ministro de Justicia, Rafael Catalá, ha condecorado a 9 procuradores con la <span class="c_blue">Orden de San Raimundo de Peñafort</span>, una de las mayores distinciones en el ámbito de la Justicia, en un acto celebrado en Madrid este miércoles en el que se premiaron los servicios y la contribución al desarrollo y perfeccionamiento del Derecho. El acto, presidido por el ministro, reconoció la aportación de profesionales del procurador tanto en el ámbito académico y normativo como en el ejercicio de sus funciones de cara al ciudadano. </p>

						<p>Desde su creación en 1944, la Orden de San Raimundo de Peñafort premia el mérito a la Justicia y recompensa hechos distinguidos o servicios relevantes, de carácter civil, en el campo del Derecho. La ceremonia contó con la presencia de la secretaria de Estado de Justicia, Carmen Sánchez-Cortés; la subsecretaria de Justicia, Áurea Roldán; y el presidente del Consejo General de Procuradores de España, Juan Carlos Estévez. </p>
						
						<div class="quote">“La Orden de San Railmundo de Peñafort premia el mérito a la Justicia por méritos...”</div>

						<p>La Cruz de San Raimundo a los ocho galardonados de distintas comunidades autónomas españolas ha sido concedidas a instancias del <span class="c_blue">Consejo General de Procuradores de España</span> (CGPE) con el objetivo de premiar los relevantes méritos contraídos por cuantos intervienen en la Administración de Justicia y en su cultivo y aplicación del estudio del Derecho en todas sus ramas. </p>

						<p>Los premiados a sido los siguientes: Dentro de la categoría <span class="c_blue">Cruz Distinguida de 1º Clase</span>: Gabriel María de Diego Quevedo, Decano del Colegio de Procuradores de Madrid; Jesús María de Fuentes Hormigo; Decano de Segovia; Rocío López González procuradora de Toledo; y Jesús Martinez Melón, Decano de Pontevedra. </p>

						<p>En la categoría <span class="c_blue">Cruz Distinguida de 2º Clase</span>: Mª José Cruz Sorribes, vicedecana del Ilustre Colegio de Procuradores de Castellón; Pilar Fuentes Tomás, procuradora e integrante de la Institución de Mediación del Consejo General de Procuradores de España; Patricia Maldiney Casasus, vicedecana de Terrasa y Enrique Sastre Bottella.</p>

						<p>Por último, en la categoría <span class="c_blue">“Medalla de Plata del Mérito a la Justicia”</span>, el premiado fue José Ramón Rego Rodríguez, procurador perteneciente al Colegio de Madrid. </p>

					</div>

						
				</div>
				<div class="noticias-sugeridas">
					<span class="sugeridas">NOTICIAS SUGERIDAS</span>
				</div>
			</div>
			
		</div>
	</section>
	<section>
		<div class="container-full noticias-slider">
			<div class="module-news">
				<div class="container-half c_left">
							
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="module-news">
				<div class="container-half c_left">
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
					
	</section>
	
<?php include("footer.php");?>