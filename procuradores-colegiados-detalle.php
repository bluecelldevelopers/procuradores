<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">COLEGIO DE PROCURADORES</span>
					<span class="subtitle">DIRECTORIO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small top">
				<span class="detalle-nombre">ALFREDO AJA GARAY</span>
				<div class="container-half">
					<div class="datos">
						<span class="ubicacion">Ilustre Colegio de Procuradores de Álava</span>
						<span class="colegiado">Núm. Colegiado: P01059000063</span>
						<span class="despacho">Despacho en Vitoria-Gasteiz</span>
					</div>
					<div class="otros">
						<div class="telefonos">
							<span class="phone"> San Prudencio, 27, 3º</span>
							<span class="phone"> 01005 - Vitoria - Álava</span>
						</div>
						<div class="telefonos">
							<span class="phone"><i class="icon icon-phone"></i> +(34) 945004899</span>
							<span class="phone"><i class="icon icon-phone"></i> +(34) 945004870</span>
						</div>
						<span class="mail"><a href="mailto: auzoaasesores@telefonica.net" "email me">auzoaasesores@telefonica.net</a></span>
					</div>
				</div><!--
				--><div class="container-half">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.335335121697!2d-3.6954346841943178!3d40.423572979364394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42288ff9c49a09%3A0x83aef272a6bc226c!2sIlustre+Colegio+Procuradores+de+Madrid!5e0!3m2!1ses!2sco!4v1485189073044" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
	</section>
	
<?php include("footer.php");?>