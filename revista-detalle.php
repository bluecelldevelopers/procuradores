<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">NÚMERO 115 · ENERO 2016</span>
					<span class="subtitle">ACTUALIDAD / REVISTA PROCURADORES</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="single-news final">
					<div class="container-half">
						<div class="revista-img-half">
							<img src="img/content/revista-bg.jpg" alt="">
						</div>	
				</div><!--
				--><div class="container-half">

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean iaculis luctus nunc ut scelerisque. Mauris vitae posuere nibh, vel porta est. Sed quis erat est. Nulla hendrerit ligula a rutrum ultricies. Phasellus in mattis massa, in tristique dui. Aenean semper maximus aliquam. Sed tristique augue sit amet mi volutpat efficitur. Sed viverra libero eget orci aliquam faucibus</p>

						<p>Sed sagittis diam a quam malesuada imperdiet vel quis felis. Nullam at urna porta, interdum dui id, finibus dui. Integer ac tellus a elit eleifend lobortis nec eu quam. Suspendisse vestibulum nisl sit amet placerat mollis. Curabitur nisi tellus, ullamcorper in orci et, consequat vulputate elit. Suspendisse lacinia, eros eget porta commodo, ligula tellus feugiat tellus, vitae egestas est lacus sed ligula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

						<div class="descargar">
							<a href="">
								<i class="icon icon-download"></i>
								<span class="text">DESCARGAR</span>
							</a>
						</div>
						<div class="ver">
							<a href="">
								<i class="icon icon-eye"></i>
								<span class="text">VER ONLINE</span>
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
	
<?php include("footer.php");?>