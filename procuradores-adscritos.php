<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">PROCURADORES ADSCRITOS AL A UIHJ</span>
					<span class="subtitle">DIRECTORIO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small top">
			<form action="" class="colegiados">
				<img src="img/content/boxes-01.jpg" alt="">
				<a href=""><span class="acceso">ACCESO A WEB UIHJ</span></a>
				<input type="text" placeholder="COLEGIO">
				<input type="text" placeholder="NOMBRE Y APELLIDOS">
				<input type="submit" class="send-btn" value="BUSCAR">
			</form>
			<div class="textos-colegiados">
				<p>Instrucciones: Puede restringir la búsqueda de nombres a la localidad de ubicación del despacho introduciendo la misma en la caja de texto. Si sólo introduce la localidad, obtendrá todos los procuradores con despacho ubicado en la localidad especificada.Si sólo introduce el nombre, obtendrá todos los procuradores de España que coincidan con el nombre especificado.</p>
			</div>
			<div class="advertencia">
				<p>En aplicación de la normativa vigente en materia de protección de datos personales, se informa que la utilización de los datos de contacto para fines comerciales, publicidad, prospección comercial u otros fines distintos del ejercicio profesional, requiere el consentimiento expreso de los colegiados, salvo los supuestos recogidos en la ley. De conformidad con lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, le informamos de que los datos que se facilitan mediante esta página, se encuentran registrados en un fichero propiedad del CONSEJO GENERAL DE LOS PROCURADORES DE ESPAÑA con la finalidad de gestión y publicación del Censo de Procuradores. Puede ejercitar sus derechos de acceso, rectificación, cancelación y oposición, mediante escrito dirigido a la dirección del Responsable del Fichero del CONSEJO GENERAL DE LOS PROCURADORES DE ESPAÑA, Calle Bárbara de Braganza nº 6; 28004-Madrid, acompañando copia de su D.N.I.</p>
			</div>

		</div>
		
	</section>
	
<?php include("footer.php");?>