		<footer>
			
				<div class="foot1">
					<div class="content">
				 		<div class="mapa">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.335335121697!2d-3.6954346841943178!3d40.423572979364394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42288ff9c49a09%3A0x83aef272a6bc226c!2sIlustre+Colegio+Procuradores+de+Madrid!5e0!3m2!1ses!2sco!4v1485189073044" width="300px" height="144" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<div class="logo">
							<img src="img/logo-footer.png" alt="">
						</div>
						<div class="copy1">
							<p>Calle Serrano Anguita, 8-10</p>
							<p>28004 Madrid</p>
						</div>
						<div class="copy2">
							<p><b>Tel. +34 913 196 848</b></p>
							<p><b>Fax +34 913 199 259</b></p>
							<p><b><a href="mailito:cgpe@cgpe.es">cgpe@cgpe.es</a></b></p>
							<div class="social">
								<ul>
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-linkedin"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="icon-up"></div>
					</div>
				</div>
				<div class="foot2">
					<div class="content">
						<span>Marcas registradas © </span>
						<div class="logos">
							<nav class="marcas">
								<ul>
									<li><a href=""><img src="img/logos-footer/mediacion.png" alt=""></a></li>
									<li><a href=""><img src="img/logos-footer/subasta.png" alt=""></a></li>
									<li><a href=""><img src="img/logos-footer/deposito.png" alt=""></a></li>
									<li><a href=""><img src="img/logos-footer/certificacion.png" alt=""></a></li>
									<li><a href=""><img src="img/logos-footer/adscritos.png" alt=""></a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<div class="foot3">
					<div class="content">
						<nav class="nav-footer">
							<ul>
								<li>© 2017 </li>
								<li><a href="">Política de privacidad</a></li>
								<li><a href="">Términos y condiciones</a></li>
								<li><a href="">Sitemap</a></li>
							</ul>
						</nav>
					</div>
				</div>
		</footer>

	 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDx6kASVAEKBZyEdofDmeGyEWGzI74VkeI&callback=initMap"
  type="text/javascript"></script>
	<script src="js/app.min.js"></script>
	<script src="js/addscripts.js"></script>
	<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
</body>
</html>