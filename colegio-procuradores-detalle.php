<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">COLEGIO DE PROCURADORES</span>
					<span class="subtitle">DIRECTORIO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small top">
			<span class="title-colegio">ILUSTRE COLEGIO DE PROCURADORES DE <span class="place">ÁLAVA</span></span>
			<nav class="menu-colegios">
				<ul class="tab-colegios">
				   <li data-tab="tab1-colegios" class="active"><a><span>CONTACTO</span></a></a></li><!--
				--><li data-tab="tab2-colegios"><a><span>ÓRGANOS</span></a></a></li><!--
				--><li data-tab="tab3-colegios"><a><span>COLEGIADOS</span></a></a></li>
				</ul>
			</nav>
				<div id="tab1-colegios" class="active">
					<span class="detalle-nombre">SEDE EN VITORIA</span>
					<div class="container-half">
						<div class="datos">
							<span class="ubicacion">Palacio Justicia.- Avda. Gasteiz,18</span>
							<span class="colegiado">1008 - Vitoria - Álava</span>
						</div>
						<div class="otros">
							<div class="telefonos">
								<span class="phone"><i class="icon icon-phone"></i> +(34) 945004899</span>
								<span class="phone"><i class="icon icon-phone"></i> +(34) 945004870</span>
							</div>
							<span class="mail"><a href="www.icpalava.es" target="_blank">www.icpalava.es</a></span>
							<span class="mail"><a href="mailto: icpalava@icpalava.es" "email me"> icpalava@icpalava.es</a></span>
						</div>
			</div><!--
			--><div class="container-half">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.335335121697!2d-3.6954346841943178!3d40.423572979364394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42288ff9c49a09%3A0x83aef272a6bc226c!2sIlustre+Colegio+Procuradores+de+Madrid!5e0!3m2!1ses!2sco!4v1485189073044" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
				</div>
				<div id="tab2-colegios">
					<span class="list-title">JUNTA DE GOBIERNO</span>
					<nav class="decanos">
					<ul>
						<li><span class="cargo">Decano</span><a href="colegio-procuradores-detalle-nombre.php"><span> ARRIETA VIERNA, JESUS MARTIN</span></a></a></li>
						<li><span class="cargo">Vicedecano</span><a href="colegio-procuradores-detalle-nombre.php"><span>SANCHEZ ALAMILLO, JULIAN</span></a></li>
						<li><span class="cargo">Secretario</span><a href="colegio-procuradores-detalle-nombre.php"><span>PALACIOS GARCIA, COVADONGA</span></a></li>
						<li><span class="cargo">Vicesecretario</span><a href="colegio-procuradores-detalle-nombre.php"><span>PEREZ-AVILA PINEDO, LUIS</span></a></a></li>
						<li><span class="cargo">Tesorero</span><a href="colegio-procuradores-detalle-nombre.php"><span>RUIZ-GOPEGUI GONZALEZ, MARIA MERCEDES</span></a></li>
						<li><span class="cargo">Vocal 1º</span><a href="colegio-procuradores-detalle-nombre.php"><span>CALVO GOMEZ, NIKOLE</span></a></li>
						<li><span class="cargo">Vocal 2º</span><a href="colegio-procuradores-detalle-nombre.php"><span>GOMEZ PEREZ DE MENDIOLA, ISABEL</span></a></li>
						<li><span class="cargo">Vocal 3º</span><span>MARCO SAENZ DE ORMIJANA, MARIA DE LAS MERCEDES</span></a></li>
						<li><span class="cargo">Vocal 4º</span><a href="colegio-procuradores-detalle-nombre.php"><span>BELTRAN ARTECHE, JOSE IGNACIO</span></a></li>
					</ul>
				</nav>
				</div>
				<div id="tab3-colegios">
					<span class="list-title">PROCURADORES COLEGIADOS</span>
					<nav class="decanos">
						<ul>
							<li><a href="colegio-procuradores-detalle-nombre.php">ALFREDO AJA GARAY</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA REGINA ANIEL-QUIROGA ORTIZ DE ZUÑIGA</span> </a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JESUS MARTIN ARRIETA VIERNA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">ALICIA ARRIZABALAGA ITURMENDI</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">PALOMA BAJO MARTINEZ DE MURGUIA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA BLANCA BAJO PALACIO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JOSE IGNACIO BELTRAN ARTECHE</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA MERCEDES BOTAS ARMENTIA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA BOULANDIER FRADE</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA SOLEDAD BURON MORILLA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JESUS MARIA CALVO BARRASA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">NIKOLE CALVO GOMEZ</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">SOLEDAD CARRANCEJA DIEZ</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">CARMEN CARRASCO ARANA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">IRATXE DAMBORENEA AGORRIA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JESUS MARIA DE LAS HERAS MIGUEL</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">FEDERICO DE MIGUEL ALONSO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">FRANCISCO JOSE MARIA DEL BELLO MARTIN</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">CARLOS JOSE ELORZA ARIZMENDI</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">PILAR ELORZA BARRERA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">OSCAR ESCAÑO ELORZA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">ANA ROSA FRADE FUENTES</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">BLANCA OLATZ GARCIA RODRIGO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">ISABEL GOMEZ PEREZ DE MENDIOLA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">RAFAEL GOMEZ-ESCOLAR CARRANCEJA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">HAIZEA GONZALEZ BARREIRA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">ITZIAR LANDA IRIZAR</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">PATRICIA LASCARAY PALACIOS</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA DE LAS MERCEDES MARCO SAENZ DE ORMIJANA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">SORAYA MARTINEZ DE LIZARDUY PORTILLO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARIA CONCEPCION MENDOZA ABAJO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">IRUNE OTERO URIA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">MARTA PAUL NUÑEZ</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">LUIS PEREZ-AVILA PINEDO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">AZUCENA RODRIGUEZ RODRIGUEZ</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JULIAN SANCHEZ ALAMILLO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">PATRICIA SANCHEZ SOBRINO</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">IÑAKI SANCHIZ CAPDEVILA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JUAN USATORRE IGLESIAS</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">JORGE FERNANDO VENEGAS GARCIA</span></a></li>
							<li><a href="colegio-procuradores-detalle-nombre.php">TOMAS RICARDO ZAPATER UNCETA</span></a></li>
						</ul>
					</nav>
				</div>
			</div>

	</section>
	
<?php include("footer.php");?>