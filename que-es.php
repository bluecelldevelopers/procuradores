<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Procuradores</title>
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0">
	<link rel="icon" href="favicon.ico">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/other-styles.css">
</head>
<body>
	<div class="wrapper">
		<header>
			<div class="container">
				<div class="box">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="mail-movil">
					<span class="icon icon-email"></span>
				</div>
				<div class="logo">
					<a href="index.php"><img src="img/logo.png" alt="Procuradores"></a>
				</div><!--
				--><nav class="menu">
					<ul>
						<li class="submenu" data-open-menu="quienes-somos">QUIÉNES SOMOS</li>
						<li class="submenu" data-open-menu="directorio">DIRECTORIO</li>
						<li class="submenu" data-open-menu="actualidad">ACTUALIDAD</li>
						<li class="submenu" data-open-menu="servicios-procurador">SERVICIOS AL PROCURADOR</li>
						<li class="submenu" data-open-menu="servicios-ciudadano">SERVICIOS AL CIUDADANO</li>
						<li class="mail"><span class="icon icon-email"></span></li>
						<li><span class="icon icon-search"></span></li>
					</ul>
					<nav class="sub s2" data-menu="quienes-somos">
						<ul class="sub-slide">
							<li><a href="que-es.php">¿QUÉ ES UN PROCURADOR?</a></li>
							<li><a href="carta-presidente.php">CARTA DEL PRESIDENTE</a></li>
							<li><a href="historia.php">HISTORIA</a></li>
							<li class="submenu" data-open-menu="organos-gobierno">ÓRGANOS DE GOBIERNO</li>
							<li class="submenu" data-open-menu="normativa">NORMATIVA</li>
							<li class="submenu" data-open-menu="nuestra-sede">CONOCE NUESTRA SEDE</li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s2" data-menu="directorio">
						<ul class="sub-slide">
							<li><a href="colegiados.php">COLEGIADOS</a></li>
							<li><a href="colegio-procuradores.php">COLEGIO DE PROCURADORES</a></li>
							<li><a href="consejos-autonomos.php">CONSEJOS AUTONÓMICOS</a></li>
							<li><a href="procuradores-adscritos.php">PROCURADORES ADSCRITOS A LA UIHJ</a></li>
							<li><a href="">ACCESO WEB A COLEGIOS</a></li>
							<li><a href="">ACCESO WEB A CONSEJOS AUTONÓMICOS</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s2" data-menu="actualidad">
						<ul class="sub-slide">
							<li><a href="informe-prensa.php">INFORMES DE PRENSA</a></li>
							<li><a href="newsletter.php">NEWSLETTER</a></li>
							<li><a href="noticias.php">NOTICIAS</a></li>
							<li><a href="revista.php">REVISTA DE PROCURADORES</a></li>
							<li><a href="memoria-anual.php">MEMORIA ANUAL</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s2" data-menu="servicios-procurador">
						<ul class="sub-slide">
							<li><a href="">VENTANILLA ÚNICA</a></li>
							<li class="submenu" data-open-menu="centro-estudios">CENTRO DE ESTUDIOS</li>
							<li class="submenu" data-open-menu="servicios-certificacion">SERVICIOS DE ACCESO CON CERTIFICADO</li>
							<li><a href="">SUBASTAS</a></li>
							<li><a href="">CORREO CORPORATIVO</a></li>
							<li><a href="">SOPORTE Y DESCARGAS</a></li>
							<li><a href="">CONVENIOS</a></li>
							<li><a href="congresos-jornadas.php">CONGRESOS Y JORNADAS</a></li>
							<li class="submenu" data-open-menu="enlaces-intereses">ENLACES DE INTERÉS</li>
							<li class="submenu" data-open-menu="informacion-institucional">INFORMACIÓN INSTITUCIONAL</li>
							<li><a href="">ZONA PRIVADA</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s2" data-menu="servicios-ciudadano">
						<ul class="sub-slide">
							<li><a href="">VENTANILLA ÚNICA</a></li>
							<li><a href="">IGUALDAD - JUSTICIA GRATUITA</a></li>
							<li><a href="">MEDIACIÓN</a></li>
							<li><a href="">SUBASTAS</a></li>
						</ul>
						<span class="icon-up"></span>
					</nav>
					<nav class="sub s3" data-menu="organos-gobierno">
						<ul class="sub-slide">
							<li><a href="organos-gobierno.php">COMITÉ EJECUTIVO</a></li>
							<li><a href="decanos-adjuntos.php">DECANOS ADJUNTOS A LA PRESIDENCIA</a></li>
							<li><a href="comision-permanente.php">COMISIÓN PERMANENTE</a></li>
							<li><a href="pleno.php">PLENO</a></li>
							<li><a href="grupos-trabajo.php">GRUPO DE TRABAJO</a></li>
							<li><a href="organigrama.php">ORGANIGRAMA</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s3" data-menu="normativa">
						<ul class="sub-slide">
							<li><a href="http://www.cgpe.es/doc/Estatuto/EstatutoG.pdf" target="_blank">ESTATUTO GENERAL</a></li>
							<li><a href="">ARCANGEL</a></li>
							<li><a href="http://www.cgpe.es/doc/Cod_Deontologico/Codigo_Deontologico.pdf" target="_blank">CÓDIDO DEONTOLÓGICO</a></li>
							<li><a href="http://www.cgpe.es/doc/ReglamentoRegimenInterior/ReglamentoRegimenInteriorCGPE.pdf" target="_blank">REGLAMENTO RÉGIMEN INTERNO</a></li>
						</ul>
						<span class="icon-up"></span>		
					</nav>
					<nav class="sub s3" data-menu="nuestra-sede">
						<ul class="sub-slide">
							<li><a href="">LOCALIZACIÓN</a></li>
							<li><a href="sede-actual.php">FOTOGALERÍA</a></li>
							<li><a href="">ALQUILER DE ESPACIOS</a></li>
						</ul>
						<span class="icon-up"></span>		
					</nav>
					<nav class="sub s3" data-menu="centro-estudios">
						<ul class="sub-slide">
							<li><a href="">CURSOS Y FORMACIÓN</a></li>
							<li><a href="">ACTIVIDADES</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s3" data-menu="servicios-certificacion">
						<ul class="sub-slide">
							<li><a href="">PLATAFORMA DE TRASLADO DE COPIAS</a></li>
							<li><a href="">PLATAFORMA DE CERTIFIACIÓN DE ENVIOS</a></li>
							<li><a href="">CONSULTA DE SALDOS Y MOV. DE CUENTAS DE CONSIGN</a></li>
							<li><a href="">INFORME DE ARANCELES</a></li>
							<li><a href="">MANUALES DE APLICACIONES INFORMÁTICAS</a></li>
							<li><a href="">OFERTAS HOTELERAS</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s3" data-menu="enlaces-intereses">
						<ul class="sub-slide">
							<li><a href="">WEB COLEGIOS</a></li>
							<li><a href="">WEB CONSEJOS AUTONOMICOS</a></li>
							<li><a href="">WEB UIHJ</a></li>
							<li><a href="">BIBLIOGRAFÍA</a></li>
							<li><a href="">OTROS</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s3" data-menu="informacion-institucional">
						<ul class="sub-slide">
							<li><a href="">ASISTENCIA JURÍDICA GRATUITA</a></li>
							<li><a href="">ACCESO A LA PROFESIÓN</a></li>
							<li class="submenu" data-open-menu="legislacion">LEGISLACIÓN</li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
					<nav class="sub s4" data-menu="legislacion">
						<ul class="sub-slide">
							<li><a href="">LEY DE ACCESO/REGLAMENTO</a></li>
							<li><a href="">LEY DE COLEGIOS PROFESIONALES</a></li>
							<li><a href="">LEY DE JUSTICIA GRATUITA</a></li>
						</ul>
						<span class="icon-up"></span>	
					</nav>
				</nav>
			</div>
		</header>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">¿QUÉ ES UN PROCURADOR?</span>
					<span class="subtitle">QUIENES SOMOS</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="vid-full">
					<div class="video-news-bg">
						<iframe width="560" height="400" src="https://www.youtube.com/embed/tegIc5zE5y0" frameborder="0" allowfullscreen></iframe>
						<div class="poster-image">
							<img src="img/content/content-03.jpg" alt="">
							<span class="icon-play"></span>	
						</div>
					</div>
				</div>
				<div class="single-news">
					
					<span class="procurador-bg">Procuradores: la garantía del justiciable</span>
					<span class="subtitle-procuradores">El procurador de ayer a hoy.</span>
					<p>El "PROCURADOR" de los romanos, el "PERSONERO" de las PARTIDAS de Alfonso X el Sabio (sgl. XII), EL PROCURADOR, desde el siglo XVIII es:</p>
					<span class="title-procuradores">QUIEN COMPARECE EN EL JUICIO EN REPRESENTACION DE OTRO.</span>
					<p>Esa representación se ha ido especializando. Hoy se diferencia entre representación legal, voluntaria y procesal. Según la actual Ley Orgánica del Poder Judicial.</p>
					<span class="title-procuradores">EL PROCURADOR ES EL REPRESENTANTE PROCESAL</span>
					<span class="subtitle-procuradores">¿Qué hacen los Procuradores?</span>
					<nav class="procuradores-nav">
						<ul>
							<li>Seguir el proceso, estar pendientes de todos los pasos y tener informados al cliente y a su abogado.</li>
							<li>Responsabilizarse de todos los trámites: recibe y firma los emplazamientos, citaciones, notificaciones, etc.; asiste a todas las diligencias y actos necesarios del pleito. Todo ello en representación y a favor de su cliente.</li>
							<li>Transmitir al abogado todos los documentos e instrucciones que lleguen a sus manos. </li>
							<li>Pagar los gastos que se generen a instancia del cliente y dar cuenta documentada de los mismos. </li>
						</ul>
					</nav>
					<span class="title-procuradores">LA RELACION ENTRE EL TRIBUNAL Y EL JUSTICIABLE SE HACE A TRAVES DEL PROCURADOR</span>
					<span class="subtitle-procuradores">¿A que está obligado un procurador?</span>
					<nav class="procuradores-nav">
						<ul>
							<li>A colaborar con los órganos jurisdiccionales en la Administración de la Justicia.</li>
							<li>A defender con profesionalidad los intereses de sus representados.</li>
							<li>A llevar documentalmente los datos precisos sobre los negocios que le haya sido encargados.</li>
							<li>A representar a los litigantes sin recursos económicos en los casos provistos por la ley.</li>
						</ul>
					</nav>
					<span class="subtitle-procuradores">¿Cómo entrar en contacto con un procurador?</span>
					<nav class="procuradores-nav">
						<ul>
							<li>En el colegio correspondiente previa petición y selección por el interesado.</li>
							<li>Según las recomendaciones del propio abogado.</li>
							<li>Por turno de oficio a requerimiento del Juzgado.</li>
						</ul>
					</nav>
					<span class="title-procuradores">UN PODER ANTE NOTARIO O ANTE EL SECRETARIO JUDICIAL PERMITE AL PROCURADOR REPRESENTAR AL INTERESADO</span>
					<span class="subtitle-procuradores">Para ejercer de procurador es necesario:</span>
					<nav class="procuradores-nav">
						<ul>
							<li>Ser Licenciado en Derecho.</li>
							<li>Obtener el título de Procurador, expedido por el Ministerio de Justicia.</li>
							<li>Colegiarse en el lugar donde se encuentre su despacho principal.</li>
							<li>Prestar juramento o promesa ante el órgano que corresponda.</li>
						</ul>
					</nav>
					<span class="subtitle-procuradores">Organización profesional:</span>
					<nav class="procuradores-nav">
						<ul>
							<li>Los Colegios son los garantes y defensores de la profesión.</li>
							<li>Es obligatorio, por ello, la inscripción de los Procuradores en el Colegio.</li>
							<li>Los Colegios se rigen por una Junta de Gobierno presidida por el Decano y elegida democráticamente.</li>
							<li>El órgano supremo de coordinación de los Colegios es el Consejo General, que junto con la Asamblea de Decanos, son los máximos órganos decisorios y rectores de los Procuradores en España.</li>
						</ul>
					</nav>
            <div class="download">
                <a href="" class="btn">Descargar <span class="icon icon-file-pdf"></span> </a>
            </div>   




				
				</div>
			</div>
		</div>
	</section>
	
<?php include("footer.php");?>