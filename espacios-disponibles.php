<?php include("header.php");?>
<section>
    <div class="main-content">
        <div class="container">
            <div class="page-title">
                <span class="title">DOSSIER ESPACIOS DISPONIBLES</span>
                <span class="subtitle">RUTA / RUTA / RUTA</span>
            </div>

        </div>
    </div>
</section>
<section>
    <div class="container-small places">
        <div class="places-Main-Img">
            <img src="img/places/main-back.png" alt="">
        </div>
        <p>
            La nueva sede del Consejo General de Procuradores de España, situada en el centro de la capital, presenta el dossier de salas
            de reuniones y formación, aquí encontrará toda la información sobre los espacios en los que realizar sus reuniones,
            cursos y eventos.
        </p>
        <div class="places-Gallery">
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>lorem</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>lorem</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>lorem</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>lorem</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>lorem</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>SALON DE ACTOS SALA 1</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
            <div class="places-Gallery-item">
                <img src="img/sede-actual-destino/sede4.jpg" alt="" class="back">
                <a target="_blank" href="espacios-disponibles-detalle.php"><div class="label">
                    <p>lorem</p>
                    <span class="icon-plus-1"></span>
                </div></a>
            </div>
        </div>
        <section>
            <div class="main-content">
                <div class="container">
                    <div class="page-title">
                        <span class="title">TARIFAS</span>
                    </div>
                </div>
            </div>
        </section>
        <div class="places-Tarifas">
            <table>
                <thead>
                    <th>SALAS</th>
                    <th>CAPACIDAD</th>
                    <th>TEATRO</th>
                    <th>ESCUELA</th>
                    <th>JORNADA COMPLETA </th>
                    <th>MEDIA JORNADA</th>
                </thead>
                <tbody>
                    <tr>
                        <td>SALA PERMANENTE</td>
                        <td>25 pax</td>
                        <td>--</td>
                        <td>--</td>
                        <td>400 €</td>
                        <td>250 €</td>
                    </tr>
                    <tr>
                        <td>SALA DE CRISTAL</td>
                        <td>10/12 pax</td>
                        <td>--</td>
                        <td>--</td>
                        <td>300 €</td>
                        <td>200 €</td>
                    </tr>
                    <tr>
                        <td>SALA VERDE</td>
                        <td>8 pax</td>
                        <td>--</td>
                        <td>--</td>
                        <td>200 €</td>
                        <td>125 €</td>
                    </tr>
                    <tr>
                        <td>SALA POLIVALENTE | FORMACIÓN</td>
                        <td>50 pax</td>
                        <td>50</td>
                        <td>25</td>
                        <td>400 €</td>
                        <td>250 €</td>
                    </tr>
                    <tr>
                        <td>SALÓN DE ACTOS</td>
                        <td>150 pax</td>
                        <td>150 pax</td>
                        <td>--</td>
                        <td>900 €</td>
                        <td>500 €</td>
                    </tr>
                    <tr>
                        <td>SALÓN DE ACTOS | SALA 1</td>
                        <td>90 pax</td>
                        <td>90 pax</td>
                        <td>--</td>
                        <td>600 €</td>
                        <td>450 €</td>
                    </tr>
                    <tr>
                        <td>SALÓN DE ACTOS | SALA 2</td>
                        <td>55 pax</td>
                        <td>55 pax</td>
                        <td>--</td>
                        <td>500 €</td>
                        <td>350 €</td>
                    </tr>
                </tbody>
            </table>
            <p>* Precios estimados sujetos al horario laboral del CGPE.</p>
        </div>
        <div class="places-tabs">
            <div class="places-tabs-tab">
                <div class="tab">
                    <p>SERVICIOS</p>
                    <span class="icon-down"></span>
                </div>
                <div style="display:none"class="content-tab">
                    <div class="col">
                        <div class="img">
                            <img src="img/places/servicios-1.png" alt="">
                        </div>
                    </div>
                    <div class="col">
                        <p class="blueboldx">Las Salas de Reunión incluyen los siguientes servicios:</p>
                        <ul>
                            <li>Montaje</li>
                            <li>Agua</li>
                            <li>Material de escritura</li>
                            <li>Flipchart</li>
                            <li>WiFi</li>
                        </ul>
                    </div>
                    <div class="col">
                        <p class="blueboldx">Servicios adicionales</p>
                        <ul>
                            <li>Coffe break</li>
                            <li>Catering</li>
                            <li>Cóctel</li>
                            <li>Flipchart</li>
                            <li>WiFi</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="places-tabs-tab">
                <div class="tab">
                    <p>PROVEEDORES DE SERVICIOS</p>
                    <span class="icon-down"></span>
                </div>
                <div style="display:none"class="content-tab twocol">
                    <div class="col">
                        <div class="img">
                            <img src="img/places/servicios-2.png" alt="">
                        </div>
                    </div>
                    <div class="col">
                        <p>Para la celebración de coffeebreak, catering y cóctel contamos con una amplia variedad de proveedores
                            de primera calidad y reconocido prestigio.</p>
                    </div>
                </div>
            </div>
            <div class="places-tabs-tab">
                <div class="tab">
                    <p>PLANO DE APARCAMIENTO CERCANO</p>
                    <span class="icon-down"></span>
                </div>
                <div style="display:none"class="content-tab onecol">
                    <div class="col">
                        <p class="blueboldx">ACCESO AL PARKING</p>
                        <p>c/ Barceló s/n, 28004 Madrid</p>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12149.341336759671!2d-3.693246!3d40.423573!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x83aef272a6bc226c!2sIlustre+Colegio+Procuradores+de+Madrid!5e0!3m2!1ses-419!2sco!4v1489772537774" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="places-tabs-tab">
                <div class="tab">
                    <p>PLANO DE LOCALIZACIÓN</p>
                    <span class="icon-down"></span>
                </div>
                <div style="display:none"class="content-tab onecol">
                    <div class="col">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12149.341336759671!2d-3.693246!3d40.423573!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x83aef272a6bc226c!2sIlustre+Colegio+Procuradores+de+Madrid!5e0!3m2!1ses-419!2sco!4v1489772537774" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("footer.php");?>