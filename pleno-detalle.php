<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">DECANOS ADJUNTOS A LA PRESIDENCIA</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNOS / PLENO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<div class="detalle-full">
				<div class="nombre-container">
					<span class="detalle-nombre">JUAN CARLOS ESTEVEZ FERNANDEZ-NOVOA</span>
				</div>
				<div class="datos">
					<span class="ubicacion">Ilustre Colegio de Procuradores de Madrid</span>
					<span class="colegiado">Núm. Colegiado: P01059000063</span>
					<span class="despacho">Despacho en Madrid</span>
				</div>
				<div class="otros">
					<span class="direccion">Enrique Lareta 7, 1ºB <br>28036 - Vitoria - Álava</span>
					<div class="telefonos">
						<span class="phone"><i class="icon icon-phone"></i> +(34) 945234488</span>
						<span class="phone"><i class="icon icon-phone"></i> +(34) 945234512</span>
					</div>
					<span class="mail"><a href="mailto: procuradores@despachoestevez.es" "email me"> procuradores@despachoestevez.es</a></span>
				</div>
				<div class="cv">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra ut ex nec commodo. Donec auctor arcu quis dolor venenatis ultrices. Phasellus nec diam a turpis facilisis tincidunt dignissim nec purus.
					</p>
					<div class="link-news">
						<a href="noticias.php"><span>VER MÁS</span></a>
					</div>
				</div>
				<div class="mapa">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.335335121697!2d-3.6954346841943178!3d40.423572979364394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42288ff9c49a09%3A0x83aef272a6bc226c!2sIlustre+Colegio+Procuradores+de+Madrid!5e0!3m2!1ses!2sco!4v1485189073044" width="100%" height="145" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		
	</section>
<?php include("footer.php");?>