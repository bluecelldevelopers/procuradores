<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">MEMORIA ANUAL</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small top">
			<div class="textos-memoria">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu libero ac nunc egestas molestie sed ut ante. Pellentesque nec orci ac nibh elementum eleifend. Donec rhoncus tincidunt augue ut gravida. Duis vitae arcu aliquet, tempor dui placerat, molestie lacus.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu libero ac nunc egestas molestie sed ut ante. Pellentesque nec orci ac nibh elementum eleifend. Donec rhoncus tincidunt augue ut gravida. Duis vitae arcu aliquet, tempor dui placerat, molestie lacus..</p>

				<div class="memoria">
					<div class="memoria1">
						<a href=""><span class="texto"><i class="icon icon-download"></i>MEMORIA ANUAL</span>
						<span class="year">2015</span></a>
					</div>
					<div class="memoria2">
						<a href=""><span class="texto"><i class="icon icon-download"></i>MEMORIA ANUAL</span>
						<span class="year">2014</span></a>
					</div>
				</div>
			</div>
		</div>
		
	</section>
	
<?php include("footer.php");?>