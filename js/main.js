(function($) {

	jQuery(document).ready(function($) {
		if ( $( "#newsmenu" ).length ){
		var waypoint = new Waypoint({
			element: document.getElementById('newsmenu'),
			handler: function(direction) {
				if(direction=="down"){
					$(".single-news").addClass("fixedmenu");
				}else if(direction=="up"){
					$(".single-news").removeClass("fixedmenu");
				}
			}
		})
	}
		if ( $( ".contentx" ).length ){
		for(i=1;i<=5;i++){
			var waypointx = new Waypoint({
				element: $('.contentx')[i-1],
				handler: function(direction) {
					id = this.element.id;
					$("."+id).siblings('li').removeClass('active');
					$("."+id).addClass('active');
					console.log(id);
				},
				offset:270
			})
		}
	}
	//Tabs
	$(".places-tabs-tab .tab").on("click",function(){
		$(this).parents(".places-tabs-tab").find(".content-tab").stop().slideToggle();
		$(this).find(".icon-down").toggleClass("active");
	});
	
		// galleries
		
		$('.principal-banner').slick({
		  dots: true,
		  arrows: false,
		  slidesToShow: 1,
		  autoplay: true,
		  autoplaySpeed: 3000
		});
		$('.noticias-slider').slick({
		  dots: false,
		  arrows: true,
		  slidesToShow: 1,
		  autoplay: true,
		  autoplaySpeed: 3000,
		  responsive: [
				{
					breakpoint: 1000,
					settings: {
					dots: true,
					arrows: false
					}
				}
			]
		});

		$('.fotogaleria-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: true,
			adaptiveHeight: true,
			asNavFor: '.fotogaleria-nav'
		});
		$.each($(".foto-slide img"),function(i,e){
			$('.fotogaleria-nav').append('<div class="slide"><img src="'+$(e).attr("src")+'" alt=""></div>');
		})
		$('.fotogaleria-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.fotogaleria-slider',
			dots: false,
			centerMode: true,
			focusOnSelect: true
		});
		/*$.each($(".grid-item img"),function(i,e){
			$('.prensa-galeria').append('<div class="slide"><img src="'+$(e).attr("src")+'" alt=""></div>');
		})*/
		$('.prensa-galeria').slick({
			dots: false,
			infinite: true,
			speed: 500,
			autoplay: false,
			autoplaySpeed: 5000,
			adaptiveHeight: true
		});

		var count = 0;

		$('.icon-plus-1.action').on('click', function(event) {
			event.preventDefault();
			var query = $(this).attr('data-query');
			var getCount = count + Number($(this).attr('data-count'));
			$.getJSON(query, function(json, textStatus) {
				$.each(json, function(index, val) {
					if(index >= count && index < getCount){
						var mod = index % 4;
						switch(mod) {
							case  0 : 
								var html = "<div class=\"congreso2\"><span class=\"titulo\">"+val.title3+"</span><span class=\"link\">"+val.link3+"</span></div><div class=\"congreso1\"><span class=\"titulo\">"+val.title2+"</span><span class=\"link\">"+val.link2+"</span></div><div class=\"congreso2\"><span class=\"titulo\">"+val.title1+"</span><span class=\"link\">"+val.link1+"</span></div><div class=\"congreso3\"><span class=\"titulo\">"+val.title+"</span><span class=\"link\">"+val.link+"</span></div>";
								break;
							case 1:
								var html = "<div class=\"congreso3\"><span class=\"titulo\">"+val.title3+"</span><span class=\"link\">"+val.link3+"</span></div><div class=\"congreso2\"><span class=\"titulo\">"+val.title2+"</span><span class=\"link\">"+val.link2+"</span></div><div class=\"congreso1\"><span class=\"titulo\">"+val.title1+"</span><span class=\"link\">"+val.link1+"</span></div><div class=\"congreso2\"><span class=\"titulo\">"+val.title+"</span><span class=\"link\">"+val.link+"</span></div>";
								break;
							case 2:
								var html = "<div class=\"congreso2\"><span class=\"titulo\">"+val.title3+"</span><span class=\"link\">"+val.link3+"</span></div><div class=\"congreso3\"><span class=\"titulo\">"+val.title2+"</span><span class=\"link\">"+val.link2+"</span></div><div class=\"congreso2\"><span class=\"titulo\">"+val.title1+"</span><span class=\"link\">"+val.link1+"</span></div><div class=\"congreso1\"><span class=\"titulo\">"+val.title+"</span><span class=\"link\">"+val.link+"</span></div>";
								break;
							case 3:
								var html = "<div class=\"congreso1\"><span class=\"titulo\">"+val.title3+"</span><span class=\"link\">"+val.link3+"</span></div><div class=\"congreso2\"><span class=\"titulo\">"+val.title2+"</span><span class=\"link\">"+val.link2+"</span></div><div class=\"congreso3\"><span class=\"titulo\">"+val.title1+"</span><span class=\"link\">"+val.link1+"</span></div><div class=\"congreso2\"><span class=\"titulo\">"+val.title+"</span><span class=\"link\">"+val.link+"</span></div>";
								break;
						}
						$('.congresos').append(html);	
					}
				});
		        count = getCount;
		    });
		});
	 	$('.open-modal').on('click',function(event){
        event.preventDefault();
        $('.modalbox').fadeIn(500);
	        $('.vid1 .prensa-galeria').slick('setPosition');
	        
	    });
	    $(".grid").on("click", ".grid-item", function() {
    		var slickindex= $(this).index();
    		$('.vid1 .prensa-galeria').slick('slickGoTo', slickindex - 1, true);
		});
	    $('.close-modal, .modalbox').on('click',function(event){
	        if($(event.target).is('.table-cell') || $(event.target).is('.close-modal')){
	        $('.modalbox').fadeOut(500, function(){
	        });
	        }
	    });
	    $.each($('.head-box img[data-src]'), function(index, val) {
	    	$(val).attr('data-oldsrc', $(val).attr('src'));
	    });
		 $('.box-info').hover(function(){ 
		 		$(this).find('img').attr('src', $(this).find('img').attr('data-src'));
			}, function(){ // hover out
			   $(this).find('img').attr('src', $(this).find('img').attr('data-oldsrc'));
			});



		// ----------- MENU --------------

		
		$('header .submenu').on('mouseover', function(event) {
			event.preventDefault();
			var dataMenu = $(this).attr('data-open-menu');
			$(this).toggleClass('active');
			$(this).siblings('.submenu').removeClass('active');
			$('header .sub[data-menu="'+dataMenu+'"]').slideToggle(500);
			
			if ($('header .sub[data-menu="'+dataMenu+'"]').hasClass('s2')) {
				$('header .sub[data-menu="'+dataMenu+'"]').siblings('.sub.s2').slideUp(500);
				$('header .sub[data-menu="'+dataMenu+'"]').siblings('.sub.s3').slideUp(500);
				$('header .sub[data-menu="'+dataMenu+'"]').siblings('.sub.s4').slideUp(500);
			} else if ($('header .sub[data-menu="'+dataMenu+'"]').hasClass('s3')) {
				$('header .sub[data-menu="'+dataMenu+'"]').siblings('.sub.s3').slideUp(500);
				$('header .sub[data-menu="'+dataMenu+'"]').siblings('.sub.s4').slideUp(500);
			} else if ($('header .sub[data-menu="'+dataMenu+'"]').hasClass('s4')) {
				$('header .sub[data-menu="'+dataMenu+'"]').siblings('.sub.s4').slideUp(500);
			}
		});	

		$('.tab-colegios li').on('click', function(event) {
		event.preventDefault();
			var tab_id = $(this).attr('data-tab'); 
			$('.container-small #'+ tab_id+'').addClass('active').siblings('div').removeClass('active');
			$(this).addClass('active');
			$(this).siblings('li').removeClass('active');
		});

		/*$('.tab-historia li').on('click', function(event) {
		event.preventDefault();
			var tab_id = $(this).attr('data-tab'); 
			$('.col-2 #'+ tab_id+'').addClass('active').siblings('div').removeClass('active');
			$(this).addClass('active');
			$(this).siblings('li').removeClass('active');
		});*/
			$('.tab-historia li').on('click', function(event) {
			event.preventDefault();
			var tab_id = $(this).attr('data-tab'); 
			$('.col-2 #'+ tab_id+'').addClass('active').siblings('div').removeClass('active');
			$('html,body').stop().animate({
			scrollTop: $("#"+tab_id).offset().top-220},
			'slow');
			$(this).addClass('active');
			$(this).siblings('li').removeClass('active');
		});

		$('.item-news .video-news .icon-play').on('click', function(event) {
			event.preventDefault();
			// console.log('hiiii');
			$(this).parent('.poster-image').fadeOut(400);
			// $('.items-news .video-news iframe')[0].src += "&autoplay=1";
			$(this).parents('.item-news').find('.video-news iframe')[0].src += "?rel=0&autoplay=1";
		});
		$('.video-news-bg .icon-play').on('click', function(event) {
			event.preventDefault();
			// console.log('hiiii');
			$(this).parent('.poster-image').fadeOut(400);
			// $('.items-news .video-news iframe')[0].src += "&autoplay=1";
			$(this).parents('.item-news').find('.video-news iframe')[0].src += "?rel=0&autoplay=1";
		});
		$('.grid').masonry({
			itemSelector: '.grid-item',
			columnWidth: '.grid-sizer',
			percentPosition: true
		});

		$('header .box').on('click', function(event) {
			event.preventDefault();
			$(this).toggleClass('box-close');
			$('header .menu').slideToggle(500);
			if ($(this).hasClass('box-close')) {
				$('.sub').slideUp(500);
			}
		});

		$('.sub .icon-up').on('click', function(event) {
			event.preventDefault();
			$(this).parents('.sub').slideUp(300);
		});

		$('footer .icon-up').on('click', function(event) {
			event.preventDefault();
			$('html, body').animate({scrollTop: 0}, 500);
		});

		if( /Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('.phrase h2').removeClass('anim-typewriter');
		}

		$('.module-news .icon-plus-1').on('click', function(event) {
			event.preventDefault();
			var newsItem = $('.module-news .container-half.c_left .item-news').first().clone();
			$('.module-news .container-half.c_left').append(newsItem);
			var newsItem2 = $('.module-news .container-half.c_right .item-news').last().clone();
			$('.module-news .container-half.c_right').append(newsItem2);
		});

		$(document).on('scroll', function(event) {
			event.preventDefault();
			
			var ScrollDocument = $(this).scrollTop();

			$('.item-news').each(function(index, el) {

				var topItem = $(el).offset().top - 700;


				if (ScrollDocument >=  topItem) {
					// $(el).fadeIn(500);
					$(el).addClass('scrollItem');
					// console.log('iguall!!!')
				}

			});

		});



	});


})(jQuery);
