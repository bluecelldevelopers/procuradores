<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">SEDE ACTUAL</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
	<div class="grid">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width2 open-modal">
			<img src="img/sede-actual/sede1-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede2-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede3-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede4-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item grid-item--width3 grid-item--height2 open-modal">
			<img src="img/sede-actual/sede5-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item grid-item--width2 grid-item--height4 open-modal">
			<img src="img/sede-actual/sede6-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede7-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede8-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede9-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede10-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede11-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item grid-item--width2 grid-item--height2 open-modal">
			<img src="img/sede-actual/sede12-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item grid-item--width2 grid-item--height2 open-modal">
			<img src="img/sede-actual/sede13-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item grid-item--height5 open-modal">
			<img src="img/sede-actual/sede21-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede14-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede15-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede16-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede17-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede18-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item open-modal">
			<img src="img/sede-actual/sede19-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		<div class="grid-item grid-item--width2 grid-item--height6 open-modal">
			<img src="img/sede-actual/sede20-1.jpg" alt="">
			<span class="subtexto">Sala de juntas</span>
		</div>
		
	</div>
	</section>
	<section class="modalbox">
		<div class="table">
			<div class="table-cell">
				<div class="container">
					<div class="vid1">
						<div class="prensa-galeria">
							<div class="slide"><img src="img/sede-actual/sede1.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede2.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede3.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede4.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede5.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede6.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede7.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede8.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede9.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede10.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede11.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede12.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede13.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede21.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede14.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede15.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede16.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede17.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede18.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede19.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede20.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede22.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede23.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede24.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede25.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede26.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede27.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede28.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede29.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede30.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede31.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede32.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede33.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede34.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede35.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede36.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede37.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede38.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede39.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede40.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede41.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede42.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede43.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede44.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede45.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede46.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede47.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede48.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede49.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede50.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede51.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede52.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede53.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede54.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede55.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede56.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede57.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede58.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede59.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede60.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede61.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede62.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede63.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede64.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede65.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede66.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede67.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede68.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede69.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede70.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede71.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede72.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede73.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede74.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede75.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede76.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede77.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede78.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede79.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede80.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede81.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede82.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede83.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede84.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede85.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede86.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede87.jpg" alt=""></div>
							<div class="slide"><img src="img/sede-actual/sede88.jpg" alt=""></div>
						</div>
						<div class="close-cont">
							<span class="close-modal"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> 
	
<?php include("footer.php");?>