<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">COMISIÓN PERMANENTE</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNOS / COMISIÓN PERMANENTE</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small top">
			<nav class="decanos final">
				<ul>
					<li><span class="cargo">Presidente</span><span> ESTEVEZ FERNANDEZ-NOVOA, JUAN CARLOS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Vicepresidente</span><span>SANCHEZ GARCIA, JAVIER CARLOS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Secretario</span><span>VILLASANTE GARCIA, JOSE MANUEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Vicesecretario</span><span>CAPDEVILA GOMEZ, MARIA DEL SOL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Tesorero</span><span>RUIZ-GOPEGUI GONZALEZ, MARIA MERCEDES</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Vicetesorero</span><span>ORTEGA ALCUBIERRE, LUIS IGNACIO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>OLIVER FERRER, LAURA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>SERRANO DOMINGUEZ, MARIA DEL MAR</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>DE CASTRO FONDEVILA, CONCEPCION</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>SALVADOR CATALAN, MANUEL ANGEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>BASARAN CONDE, MARIA BELEN</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>RODRIGUEZ LOPEZ, MIGUEL ANDRES</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>TOUS ESTANY, MARIA DEL PILAR</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>BUSTAMANTE ESPARZA, PABLO ANTONIO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>HERRERA GOMEZ, ISABEL MARIA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>IZAGUIRRE OYARBIDE, NATIVIDAD</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ESCARTIN GARCIA DE CECA, MARIA ISABEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GARCIA GONZALEZ, INMACULADA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>JIMENEZ HERRERO, MARIA TERESA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ETXABE AZKUE, SAIOA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>SOBRINO CORTES, CARLOS JAVIER</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>HERRERO JIMENEZ, MARIA INGRID</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ESCRIBANO AYLLON, SERGIO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ALVAREZ MURIAS, ANA MARIA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>DE DIEGO QUEVEDO, GABRIEL MARIA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>VALERO HERNANDEZ, MARIA LUISA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CARRASCO MUÑOZ, FERNANDO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ARRIETA VIERNA, JESUS MARTIN</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GARCIA DEL CASTILLO, CLAUDIO JESUS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GOMEZ MONTEAGUDO, LORENZO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GONZALEZ-CONCHEIRO ALVAREZ, FERNANDO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ZUAZO CERECEDA, MARIA TERESA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CARRASCO ARCE, JOSE RAMON</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GARRIDO RODRIGUEZ, RICARDO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ALONSO GARCIA, OSCAR</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GONZALEZ GONZALEZ, JOSE MANUEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GUTIERREZ BENITO, ELIAS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ORTIZ MORA, ANTONIO JAVIER</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>LOZANO CONESA, ALEJANDRO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CORTES RAMIREZ, MARIA JOSE</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>PARDO PAZ, JOSE ANGEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>DOMINGO LLAO, FEDERICO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>LOZANO ADAME, MARIA DE LA CONCEPCION</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>NAVARRO ZAPATER, RAMIRO SIXTO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>BELDERRAIN GARCIA, ANA CECILIA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CASTELLS LOPEZ, MANUEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ALMEIDA LORENCES, JUAN CARLOS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>FERNANDEZ-MIJARES SANCHEZ, LAURA DEL SOCORRO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CUARTERO ALONSO, MARIA NIEVES</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>JUAN VICEDO, GINES</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GOICOECHEA TORRES, MARIA CRISTINA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>LOPEZ CHOCARRO, IGNACIO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CUCURULL HANSEN, DAMIAN</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>MUÑOZ CORREA, OSCAR</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>NAVARRO-RUBIO TROISFONTAINES, MONICA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>FUENTE HORMIGO, JESUS MARIA DE LA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GARCIA CLAVEL, ESTER</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>MARTINEZ MELON, JESUS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ORTEGA ALCUBIERRE, LUIS IGNACIO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>RONCERO AGUILA, ANTONIO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>SANTAMARIA ALCALDE, FERNANDO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GARCIA GARCIA, MIGUEL ANGEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GAZQUEZ ALCOBA, MARIA DEL MAR</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>TABERNE JUNQUITO, ANDRES</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>SANCHEZ GARCIA, JAVIER CARLOS</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GARRIDO RODRIGUEZ, RICARDO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>ALVAREZ ALBARRAN, LUIS GONZALO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>AZORIN GARCIA, MANUEL FRANCISCO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>RUBIO ANTONIO, CARMEN</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>GAGO RODRIGUEZ, JUAN MANUEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>HURTADO LOPEZ, JOSE MIGUEL</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CRUZ LLEDO, ENRIQUE DE LA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CALVAREZ TEJERINA, FERNANDO</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>RUIZ GALMES, FREDERIC XAVIER</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>TERRADAS CUMALAT, ANNA MARIA</span><span class="autonomia">AUTONOMÍA</span></li>
					<li><span class="cargo">Consejero</span><span>CAPDEVILA GOMEZ, MARIA DEL SOL</span><span class="autonomia">AUTONOMÍA</span></li>

				</ul>
			</nav>
		</div>

	</section>
	
<?php include("footer.php");?>