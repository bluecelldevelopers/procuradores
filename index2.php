<?php include("header.php");?>
	<section>
		<div class="principal-banner">
			<div class="slide-banner">
				<img src="img/content/banner-principal.jpg" alt="">
				<div class="text-slide">
					<h2>Mar de Plata, Argentina sede de un encuentro internacional sobre "Buenas prácticas en la Ejecución"</h2>
					<span class="more"><a href="">LEER ARTÍCULO</a></span>
				</div>
			</div>
			<div class="slide-banner">
				<img src="img/content/banner-principal.jpg" alt="">
				<div class="text-slide">
					<h2>Mar de Plata, Argentina sede de un encuentro internacional sobre "Buenas prácticas en la Ejecución"</h2>
					<span class="more"><a href="">LEER ARTÍCULO</a></span>
				</div>
			</div>
			<div class="slide-banner">
				<img src="img/content/banner-principal.jpg" alt="">
				<div class="text-slide">
					<h2>Mar de Plata, Argentina sede de un encuentro internacional sobre "Buenas prácticas en la Ejecución"</h2>
					<span class="more"><a href="">LEER ARTÍCULO</a></span>
				</div>
			</div>
		</div>
	</section>
	<section>
	
			<div class="module-boxes">
				<div class="box-info">
					<div class="image-background">
						<img src="img/content/imgbox-03.jpg" alt="">
					</div>
					<div class="text-box">
						<div class="head-box">
							<h3>¿Qué es un procurador?</h3>
						</div>
						<span><a href="">LEER MÁS</a></span>
					</div>
				</div><!--
				--><div class="box-info">
					
					<div class="text-box">
						<div class="head-box">
							<img src="img/content/boxes-03.jpg" alt="">
						</div>
						<span><a href="">IR A LA WEB</a></span>
					</div>
				</div><!--
				--><div class="box-info">
					<div class="image-background">
						<img src="img/content/imgbox-02.jpg" alt="">
					</div>
					<div class="text-box">
						<div class="head-box">
							<h3>Centro de estudios</h3>
						</div>
						<span><a href="">MÁS INFORMACIÓN</a></span>
					</div>
				</div><!--
				--><div class="box-info">
				
					<div class="text-box">
						<div class="head-box">
							<img src="img/content/boxes-02.jpg" alt="">
						</div>
						<span><a href="">ACCEDER</a></span>
					</div>
				</div><!--
				--><div class="box-info">
					<div class="image-background">
						<img src="img/content/imgbox-04.jpg" alt="">
					</div>
					<div class="text-box">
						<div class="head-box">
							<h3>Plataforma telemática</h3>
						</div>
						<span><a href="">MÁS INFORMACIÓN</a></span>
					</div>
				</div><!--
				--><div class="box-info">
					
					<div class="text-box">
						<div class="head-box">
							<img src="img/content/boxes-01.jpg" alt="">
						</div>
						<span><a href="">MÁS INFORMACIÓN</a></span>
					</div>
				</div><!--
				--><div class="box-info">
					<div class="image-background">
						<img src="img/content/imgbox-01.jpg" alt="">
					</div>
					<div class="text-box">
						<div class="head-box">
							<h3>Certificación de envío</h3>
						</div>
						<span><a href="">MÁS INFORMACIÓN</a></span>
					</div>
				</div>
			</div>
		
	</section>
	<section class="mt40">
		<div class="container">
			<div class="phrase">
				<h2 class="anim-typewriter">“El procurador está al servicio del ciudadano y la justicia”</h2>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news mb40">
				<h2 class="principal-title">NOTICIAS</h2>
				<div class="container-news">
					<div class="item-news image">
						<div class="image-news">
							<img src="img/content/content-01.jpg" alt="">
							<div class="link-news">
								<h4>DESTACADO</h4>
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
						<div class="text-news">
							<h2>El Consejo General de Procuradores de España otorga el premio Excelencia y Calidad de la Justicia a Antonio Fernández Buján y Manuel Altaba</h2>
						</div>
					</div>
					<div class="item-news text">
						<div class="text-news">
							<h2>El Consejo General de Procuradores de España otorga el premio Excelencia y Calidad de la Justicia a Antonio Fernández Buján y Manuel Altaba</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					<div class="item-news video">
						<div class="video-news">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/Qd2JZw7eCrk" frameborder="0" allowfullscreen></iframe>
							<div class="poster-image">
								<img src="img/content/content-03.jpg" alt="">
								<span class="icon-play"></span>	
							</div>
						</div>
						<div class="text-news">
							<h2>Juan Carlos Estévez: "El final del papel en la Justicia es irreversible”</h2>
							<div class="link-news">
								<h4>DESTACADO</h4>
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					<div class="item-news text ">
						<div class="text-news">
							<h2>El Consejo General de Procuradores de España otorga el premio Excelencia y Calidad de la Justicia a Antonio Fernández Buján y Manuel Altaba</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					<div class="item-news image ">
						<div class="image-news">
							<img src="img/content/content-01.jpg" alt="">
							<div class="link-news">
								<h4>DESTACADO</h4>
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
						<div class="text-news">
							<h2>El Consejo General de Procuradores de España otorga el premio Excelencia y Calidad de la Justicia a Antonio Fernández Buján y Manuel Altaba</h2>
						</div>
					</div>
					<div class="item-news video ">
						<div class="video-news">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/Qd2JZw7eCrk" frameborder="0" allowfullscreen></iframe>
							<div class="poster-image">
								<img src="img/content/content-02.jpg" alt="">
								<span class="icon-play"></span>	
							</div>
						</div>
						<div class="text-news">
							<h2>Juan Carlos Estévez: "El final del papel en la Justicia es irreversible”</h2>
							<div class="link-news">
								<h4>DESTACADO</h4>
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
				<div class="icon-plus-1"></div>
			</div>
		</div>
	</section>
<?php include("footer.php");?>