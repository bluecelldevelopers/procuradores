<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">CONGRESOS Y JORNADAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section class="top">
		<div class="container-half">
			<div class="mod1">
				<span class="titulo">VI Congreso del Observatorio contra la Violencia Doméstica y de Género</span>
				<span class="fecha">3 y 4 de noviembre de 2016</span>
				<span class="lugar"><i class="icon icon-location"></i>Antiguo Salón de Sesiones del Senado - MADRID</span>
			</div>
		</div><!--
		--><div class="container-half">
				<div class="mod2">
					<span class="titulo">El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</span>
					<span class="fecha">23, 24, 25 de septiembre de 2016</span>
					<span class="lugar"><i class="icon icon-location"></i>BURGOS - CASTILLA Y LEÓN</span>
				</div>
			</div>
	</section>
	<section>
		<div class="congresos">
			<div class="congreso1">
				<span class="titulo">Catalá anuncia cambios en los horarios y días de celebración de las subastas electrónicas </span>
				<span class="link">BILBAO - XIV CONGRESO NACIONAL DE PROCURADORES</span>
			</div><!--
			--><div class="congreso2">
				<span class="titulo">Juan Carlos Estévez: Hoy es un gran día para los Procuradores </span>
				<span class="link">BILBAO - XIV CONGRESO NACIONAL DE PROCURADORES</span>
			</div><!--
			--><div class="congreso3">
					<span class="titulo">XIV Congreso Nacional de Procuradores</span>
				<span class="link">BILBAO - XIV CONGRESO NACIONAL DE PROCURADORES</span>
				</div><!--
			--><div class="congreso4">
					<span class="titulo">V Jornadas Nacionales entre Letrados de la Administración de Justicia y los Procuradores</span>
				<span class="link">NOTA DE PRENSA</span>
				</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="icon-plus-1 action" data-query="json/module.json" data-count="1"></div>
			</div>
		</div>	
	</section>
	
<?php include("footer.php");?>