<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">PLENO</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNOS / PLENO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small top">
			<nav class="decanos final">
				<ul>
					<li><span class="cargo">Presidente</span><a href="pleno-detalle.php"><span> <b>ESTEVEZ FERNANDEZ-NOVOA, JUAN CARLOS</b></span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Vicepresidente</span><a href="pleno-detalle.php"><span>SANCHEZ GARCIA, JAVIER CARLOS</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Secretario</span><a href="pleno-detalle.php"><span>VILLASANTE GARCIA, JOSE MANUEL</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Vicesecretario</span><a href="pleno-detalle.php"><span>CAPDEVILA GOMEZ, MARIA DEL SOL</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Tesorero</span><a href="pleno-detalle.php"><span>RUIZ-GOPEGUI GONZALEZ, MARIA MERCEDES</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Vicetesorero</span><a href="pleno-detalle.php"><span>ORTEGA ALCUBIERRE, LUIS IGNACIO</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>BUSTAMANTE ESPARZA, PABLO ANTONIO</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>OLIVER FERRER, LAURA</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>HERRERA GOMEZ, ISABEL MARIA</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>IZAGUIRRE OYARBIDE, NATIVIDAD</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>GARRIDO RODRIGUEZ, RICARDO</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>HERRERO JIMENEZ, MARIA INGRID</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>ALVAREZ MURIAS, ANA MARIA</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>DE DIEGO QUEVEDO, GABRIEL MARIA</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>GARCIA DEL CASTILLO, CLAUDIO JESUS</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>BELDERRAIN GARCIA, ANA CECILIA</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>ZUAZO CERECEDA, MARIA TERESA</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><span>ALONSO GARCIA, OSCAR</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>GARCIA GARCIA, MIGUEL ANGEL</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>ALMEIDA LORENCES, JUAN CARLOS</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><span>LOPEZ CHOCARRO, IGNACIO</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><span>PARDO PAZ, JOSE ANGEL</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>ORTEGA ALCUBIERRE, LUIS IGNACIO</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>SANTAMARIA ALCALDE, FERNANDO</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>RUIZ GALMES, FREDERIC XAVIER</span></a><span class="colegio-pleno">COLEGIO</span></li>
					<li><span class="cargo">Consejero</span><a href="pleno-detalle.php"><span>GONZALEZ CONESA, MILAGROSA</span></a></a><span class="colegio-pleno">COLEGIO</span></li>
				</ul>
			</nav>
		</div>

	</section>
	
<?php include("footer.php");?>