<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">GRUPOS DE TRABAJO DE FORMACIÓN Y CULTURA</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNO /GRUPOS DE TRABAJO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
				<div class="container-small top">
			<nav class="decanos final">
				<ul>
					<li><span class="cargo">Miembro</span><a href="grupos-detalle-nombre.php"><span>ALVAREZ-BUYLLA BALLESTEROS, MANUEL MARIA</span></a></li>
					<li><span class="cargo">Miembro</span><span>MAMPEL TUSELL, MARIA DEL PILAR</span></li>
					<li><a href="grupos-detalle-nombre.php"><span class="cargo">Miembro</span><span>GOICOECHEA TORRES, MARIA CRISTINA</span></a>
					</li>
					<li><span class="cargo">Miembro</span><a href="grupos-detalle-nombre.php"><span>TABERNE JUNQUITO, ANDRES</span></a></li>
					<li><span class="cargo">Miembro</span><a href="grupos-detalle-nombre.php"><span>RUIZ-GOPEGUI GONZALEZ, MARIA MERCEDES</span></a></li>
					<li><span class="cargo">Miembro</span><a href="grupos-detalle-nombre.php"><span>CABALLERO AGUADO, JULIAN</span></a></li>
				</ul>
			</nav>
		</div>
	</section>
	
<?php include("footer.php");?>