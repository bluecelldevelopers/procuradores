<?php include("header.php");?>
<section>
    <div class="main-content">
        <div class="container">
            <div class="page-title">
                <span class="title">DOSSIER ESPACIOS DISPONIBLES</span>
                <span class="subtitle">RUTA / RUTA / RUTA</span>
            </div>

        </div>
    </div>
</section>
<section>
    
    <div class="container-small places">
<section>
    <div class="main-content">
        <div class="container">
            <div class="page-title" style="text-align: left;">
                <span class="title">SALA PERMANENTE</span>
            </div>

        </div>
    </div>
</section>
        <div class="places-tabs">
            <div class="places-tabs-tab">
                <div class="content-tab twocol">
                    <div class="col" style="width: 48% !important;">
                        <div class="img">
                            <img src="img/places/sala-1.jpg" alt="">
                        </div>
                    </div>
                    <div class="col font" style="width: 62% !important;    padding-left: 2em;">
                        <p class="blueboldx" style="margin-left:0 !important;">Descripción</p>
                        <p>Esta sala de reuniones se encuentra ubicada en la planta primera, equipada con un mobiliario sobrio
                            y elegante, ideal para realizar reuniones de negocios, así como presentaciones a la prensa.</p>

                        <p class="blueboldx" style="margin-left:0 !important;">Características</p>
                        <p> - Superficie: 75 m2 <br> - Altura: 2,50 m <br> - Longitud aprox.:12,60 m <br> - Ancho aprox.: 5,94
                            m</p>

                        <p class="blueboldx" style="margin-left:0 !important;">Formatos:</p>
                        <p>- Reunión: 25 pax</p>

                        <p class="blueboldx" style="margin-left:0 !important;">Equipamiento:</p>
                        <p>- Mesa fija de madera lacada marrón y piel con pie de acero cromado. <br> - Sillones de piel.<br>                            - Conexión WiFi.<br> - 6 puntos de luz y toma de red integrados en la mesa. </p>

                        <p class="blueboldx" style="margin-left:0 !important;">Servicios opcionales:</p>
                        <p>- Pantalla y proyector.</p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("footer.php");?>