<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">NOTICIAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="container-half c_left border">
					<div class="item-news image">
						<div class="image-news">
							<img src="img/content/content-01.jpg" alt="">
							<div class="link-news">
								<h4>DESTACADO</h4>
								<a href="noticias-detalle.php"><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
						<div class="text-news">
							<h2>El Consejo General de Procuradores de España otorga el premio Excelencia y Calidad de la Justicia a Antonio Fernández Buján y Manuel Altaba</h2>
							<p>Antonio Fernández-Buján, Catedrático de Derecho Romano de la Universidad Autónoma de Madrid y Manuel Altaba Lavall, Senador por el Partido Popular, recibieron el Premio “Excelencia y en la Justicia 2015” ...</p>		
						</div>
					</div>
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>El Consejo General de Procuradores organizó este jueves un nuevo curso técnico de formación al que asistieron más de 80 empleados...</p>
							<div class="link-news">
								<a href="noticias-detalle.php"><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					<div class="item-news text">
						<div class="text-news">
							<h2>Manuel Marchena visita la nueva sede del CGPE y se entrevistó con Juan Carlos Estévez.</h2>
							<p>El presidente de la Sala de lo Penal del Tribunal Supremo (TS), Manuel Marchena, visito este martes la nueva sede del Consejo...</p>
							<div class="link-news">
								<a href=""><span>VER FOTOGALERÍA</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right">
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href="noticias-detalle.php"><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					<div class="item-news video ">
						<div class="video-news">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/Qd2JZw7eCrk" frameborder="0" allowfullscreen></iframe>
							<div class="poster-image">
								<img src="img/content/content-02.jpg" alt="">
								<span class="icon-play"></span>	
							</div>
						</div>
						<div class="text-news">
							<h2>Juan Carlos Estévez: "El final del papel en la Justicia es irreversible”</h2>
							<div class="link-news">
								<h4>DESTACADO</h4>
								<a href="noticias-detalle.php"><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href="noticias-detalle.php"><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
					
					
				</div>
				<div class="icon-plus-1"></div>
			</div>
		</div>
	</section>
	
<?php include("footer.php");?>