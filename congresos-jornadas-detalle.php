<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">CONGRESOS Y JORNADAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<div class="module-news">
				<div class="single-news">
					<span class="news-title">Catalá anuncia cambios en los horarios y días de celebración de las subastas electrónicas</span>
					
					<div class="image-news-detail">
						<img src="img/content/content-09.jpg" alt="">
					</div>
					<span class="pie">Manuel Marchena, presidente de la Sala de lo Penal del Tribunal Supremo y Juan Carlos Estévez, presidente del CGPE.</span>
					<div class="fecha-cont">
						<span class="fecha">27/07/2016</span>
					</div>
					<span class="tags">Premios, Universidad, Procuradores</span>
					<div class="text-content">
						<p>En su intervención, Catalá ha subrayado la importancia de la función de estos profesionales como pieza clave en nuestro sistema jurídico y su decidido apoyo a las reformas emprendidas durante la pasada legislatura por el Gobierno.</p>

						<p>Así, el ministro de Justicia ha hecho referencia al amplio programa de reformas en el ámbito de las leyes procesales acometido durante la X Legislatura y ha destacado el trabajo realizado para llegar a acuerdos por parte del Consejo General de Procuradores, que ha prestado un constante apoyo a esas reformas necesarias sin dejar de defender firmemente sus derechos.En esa línea, el titular de Justicia ha puesto como ejemplo del buen entendimiento entre los procuradores y el Ministerio la asunción por parte de éste de las peticiones planteadas en torno a las subastas electrónicas.</p>

						<p>De esta forma, en adelante comenzarán a las 18:00 horas en lugar de a las 24:00 horas como se venía haciendo desde que arrancó el portal hace ya medio año. De la misma manera, las subastas terminarán también a las seis de la tarde siempre que no se produzca su prórroga por formalizarse las pujas durante la hora previa a la conclusión, ya que la mejor puja ha de permanecer una hora sin ser superada. Además, tal y como se reclamaba, los anuncios de subasta serán publicados de manera que no determinen la conclusión en sábado o domingo.</p>

						<p>Rafael Catalá se ha referido a algunas de las reformas llevadas a cabo y que han afectado al ejercicio del procurador, entre las que ha señalado la Ley de Enjuiciamiento Civil que les faculta para realizar los actos de comunicación junto con los funcionarios del Cuerpo de Auxilio Judicial, con lo que se establece una nueva relación de colaboración de este colectivo con la Administración de Justicia.</p>

						<p>El ministro de Justicia ha reconocido el apoyo de los procuradores para avanzar en los procesos de agilización de las comunicaciones y para conseguir el objetivo de una Justicia abierta, digital e innovadora que tuvo un hito importante el pasado día 1 de enero, fecha de inicio de la obligatoriedad para que los profesionales y órganos judiciales emplearan los sistemas telemáticos existentes en la Administración de Justicia para la presentación de escritos y documentos y para la realización de actos de comunicación procesal.</p>

						<p>Rafael Catalá ha agradecido el papel protagonista de estos profesionales que llevan años trabajando con estas tecnologías que han permitido un importante ahorro a la Administración, a los ciudadanos y a los profesionales de la Justicia y que en el caso de éstos se cifra en casi 140 millones de euros en lo que llevamos de año.</p>

					</div>

						
				</div>
				
			</div>
			<span class="sugeridas">NOTICIAS SUGERIDAS</span>
		</div>
	</section>
	<section>
		<div class="container-full noticias-slider">
			<div class="module-news">
				<div class="container-half c_left">
							
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="module-news">
				<div class="container-half c_left">
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
					
	</section>
	
<?php include("footer.php");?>