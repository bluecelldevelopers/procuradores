<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">CONGRESOS Y JORNADAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section class="top">
		<div class="container-half">
			<div class="mod1 fix-height">
				<span class="titulo">VI Congreso del Observatorio contra la Violencia Doméstica y de Género</span>
				<span class="fecha">3 y 4 de noviembre de 2016</span>
				<span class="lugar"><i class="icon icon-location"></i>Antiguo Salón de Sesiones del Senado - MADRID</span>
			</div>
		</div><!--
		--><div class="container-half">
				<div class="congresos no-margin">
					<div class="congreso3">
						<span class="titulo">Firma Convenio de colaboración entre el CGPE y El Banco Popular </span>
						<span class="link">DESCARGAR INFORME DE PRENSA</span>
					</div><!--
					--><div class="congreso2">
						<span class="titulo">Juan Carlos Estévez: Hoy es un gran día para los Procuradores </span>
						<span class="link">
							<nav>
							    <ul>
							        <li><a href="#">MEDIO 1 | </a></li><!--
							     --><li><a href="#">MEDIO 2 | </a></li><!--
							     --><li><a href="#">MEDIO 3</a></li>
							    </ul>
							</nav>
						</span>
					</div>
				</div>
			</div>
	</section>
	<section>
		<div class="congresos">

			<div class="congreso3">
					<span class="titulo">Suspendisse vel tempor felis </span>
					<span class="link">
						<nav>
						    <ul>
						        <li><a href="#">MEDIO 1 | </a></li><!--
						     --><li><a href="#">MEDIO 2 | </a></li><!--
						     --><li><a href="#">MEDIO 3</a></li>
						    </ul>
						</nav>
					</span>
				</div><!--
			--><div class="congreso2">
				<span class="titulo">Curabitur libero metus, molestie et iaculis eget</span>
				<span class="link">
					<nav>
					    <ul>
					        <li><a href="#">MEDIO 1 | </a></li><!--
					     --><li><a href="#">MEDIO 2 | </a></li><!--
					     --><li><a href="#">MEDIO 3</a></li>
					    </ul>
					</nav>
				</span>
			</div><!--
			--><div class="congreso1">
				<span class="titulo">Vestibulum interdum, risus et fermentum facilisis, mi purus aliquet ex</span>
				<span class="link">
					<nav>
					    <ul>
					        <li><a href="#">MEDIO 1 | </a></li><!--
					     --><li><a href="#">MEDIO 2 | </a></li><!--
					     --><li><a href="#">MEDIO 3</a></li>
					    </ul>
					</nav>
				</span>
			</div><!--
			--><div class="congreso4">
					<span class="titulo">Aliquam ut arcu maximus nisi volutpat</span>
					<span class="link">
						<nav>
						    <ul>
						        <li><a href="#">MEDIO 1 | </a></li><!--
						     --><li><a href="#">MEDIO 2 | </a></li><!--
						     --><li><a href="#">MEDIO 3</a></li>
						    </ul>
						</nav>
					</span>
				</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="icon-plus-1"></div>
			</div>
		</div>	
	</section>
	
<?php include("footer.php");?>