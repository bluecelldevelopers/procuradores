<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">CONOCE NUESTRA SEDE</span>
					<span class="subtitle">QUIENES SOMOS</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
	<div class="container-small top">
		<img src="img/content/content-06.jpg" alt="">
		<div class="sede">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet ipsum non est mattis dictum quis eget velit. Nullam nec orci purus. Etiam ultricies nulla quis congue condimentum. Quisque ut porttitor ante. Etiam sit amet consectetur quam, sed tempus gula. Sed posuere, lectus in interdum feugiat, ex velit viverra massa, nec ultrices orci erat in nunc. Donec placerat nulla eu sapien mollis accumsan. In hac habitasse platea dictumst.</p>
		</div>

		<div class="sede-modulos">
				<div class="modulo1">
					<span class="icono"><i class="icon icon-location"></i> </span>
					<span class="texto">C/ Bárbara de Braganza 6 28004 <strong>Madrid</strong></span>

				</div><!--
			--><div class="modulo2">
					<span class="icono"><i class="icon icon-phone"></i> </span>
					<span class="texto">+34 913 196 848</span>
				</div><!--
			--><div class="modulo3">
					<span class="icono"><i class="icon icon-email"></i> </span>
					<span class="texto"><a href="mailto:cgpe@cgpe.es" "email me">cgpe@cgpe.es</a></span>
				</div>
			</div>
			<div class="top final">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.1675690245092!2d-3.6998497844203406!3d40.427288262920804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42288ff9cd6b45%3A0xcbc89fcb838495bf!2sConsejo+General+de+Procuradores+de+Espa%C3%B1a!5e0!3m2!1ses!2sco!4v1485204630651" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
	
<?php include("footer.php");?>