<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">CONSEJOS AUTONÓMICOS</span>
					<span class="subtitle">DIRECTORIO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container">
			<div class="cont-sm">
				<nav class="decanos top final">
					<ul>
						<li><span>CONSEJO<span class="place">ANDALUZ </span> DE COLEGIOS DE PROCURADORES DE LOS TRIBUNALESALAVA</span></li>
						<li><span>CONSEJO<span class="place">CANARIO </span>CANARIO DE COLEGIOS DE PROCURADORES</span></li>
						<li><span>CONSEJO DE COLEGIOS DE PROCURADORES DE LOS TRIBUNALES DE<span class="place">ARAGON</span></span></li>
						<li><span>CONSEJO DE LOS II. COLEGIOS DE PROCURADORES DE <span class="place">CASTILLA-LA MANCHA</span></span></li>
						<li><span>CONSEJO<span class="place">VALENCIANO </span> DE COLEGIOS DE PROCURADORES DE LA <span class="place">COMUNIDAD DE VALENCIA</span></span></li>
						<li><span>CONSELL DE COL.LEGIS DE PROCURADORS DELS TRIBUNALS DE <span class="place">CATALUNYA</span></span></li>
						<li><span>CONSELLO GALEGO DOS PROCURADORES DOS TRIBUNAIS DEF  <span class="place">GALICIA</span></span></li>
					</ul>
				</nav>
			</div>
		</div>

	</section>
	
<?php include("footer.php");?>