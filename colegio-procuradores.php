<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">COLEGIO DE PROCURADORES</span>
					<span class="subtitle">DIRECTORIO</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<nav class="decanos top final colegio">
				<ul>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ALAVA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ALBACETE</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ALICANTE</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ALMERIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ANTEQUERA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">AVILA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">BADAJOZ</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">BALEARES</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">BARCELONA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">BURGOS</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CACERES</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CADIZ</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CANTABRIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CARTAGENA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CASTELLON</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CEUTA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CIUDAD REAL</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CORDOBA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">CUENCA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ELCHE</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">GERONA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">GIJON</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">GRANADA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">GUADALAJARA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">GUIPUZCOA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">HUELVA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">HUESCA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">JAEN</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">JEREZ DE LA FRONTERA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> LA CORUÑA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">LA RIOJA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">LAS PALMAS DE G. CANARIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">LEON</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">LERIDA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">LORCA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">LUGO</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MADRID</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MALAGA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MANRESA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MATARO</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MELILLA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MURCIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">NAVARRA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">ORENSE</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">OVIEDO</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">PALENCIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">PONTEVEDRA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">REUS</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">SALAMANCA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">MALAGA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">SANTIAGO DE COMPOSTELA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">SEGOVIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> SEVILLA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place">SORIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> TARRAGONA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> TENERIFE</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> TERRASSA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> TERUEL</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> TOLEDO</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> TORTOSA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> VALDEPEÑAS</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> VALENCIA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> VALLADOLID</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> VIGO</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> VIZCAYA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> YECLA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> ZAMORA</span></span></li>
					<li><span class="after">ILUSTRE COLEGIO DE PROCURADORES DE<span class="place"> ZARAGOZA</span></span></li>

				</ul>
			</nav>
		</div>

	</section>
	
<?php include("footer.php");?>