(function($){
	jQuery(document).ready(function($) {
		/*document*/
			$(document).on('click', function(event) {
				if($(event.target).closest('.tool-block').length == 0){
					if($(event.target).is('.open-tool') || $(event.target).is('.vote-btn'))
						return;
					else
						$('.tool-block').stop().slideUp(300);
				}
			});
		/*more ctrl action*/
			$('.more-ctrl[data-showByElements]').each(function(index, el) {
				var len_ = $(el).attr('data-showByElements');
				var focus = $(el).attr('data-focus');
				switch(focus){
					case "artist":
						var itemsLen = $('.artist-section').children().length;
						len_ <= itemsLen ?  $('.more-ctrl').addClass('show') : "";
					break;
					case "news":
						var itemsLen = $('.news-section .news-block').length;
						len_ <= itemsLen ?  $('.more-ctrl').addClass('show') : "";
					break; 
					case "events":
						var itemsLen = $('.events-total-section .event-block').length;
						len_ <= itemsLen ?  $('.more-ctrl').addClass('show') : "";
					break; 
				}
			});
			var countItems = 0;
			$('.more-ctrl[data-query]').on('click', function(event) {
				event.preventDefault();
				var url = $(this).attr('data-query');
				var focus = $(this).attr('data-focus');
				switch(focus){
					case "artist":
						var itemLen = 4;
						$.getJSON(url, function(json, textStatus) {
							$.each(json, function(index, val) {
								if(index >= countItems && index < (countItems+itemLen)){
									var html = '<div class="artist animated fadeInUp"><img src="'+val.imgURL+'" alt=""><div class="hover"><p class="name">'+val.name+'</p><span class="icon icon-arrow-right"></span></div><a href="'+val.href+'" class="link"></a></div>';
									$('.artist-section').append(html);
								}
							});
							countItems += itemLen;
						});
					break;
					case "news":
						var itemLen = 4;
						if($('.news-section').is('.styled-fixed')){
							$.getJSON(url, function(json, textStatus) {
								$.each(json, function(index, val) {
									if(index >= countItems && index < (countItems+itemLen)){
										var html = '<div class="news-block animated fadeInUp"><a href="'+val.href1+'"><figure class="cover-img"><img src="'+val.imgURL1+'" alt=""><figcaption><p>'+val.title1+'</p><span class="icon icon-arrow-right"></span></figcaption></figure></a></div>';
										$('.news-section.styled-fixed .news-block').removeClass('last-block mobile-last-block');
										$('.news-section .column').append(html);
										var news_len = $('.news-section.styled-fixed .news-block').length;
										if(news_len % 2 && news_len >= 5){
											$('.news-section.styled-fixed .news-block').slice(-3).addClass('last-block');
											$('.news-section.styled-fixed .news-block:last-child').addClass('mobile-last-block');
										}
									}
								});
								countItems += itemLen;
							});
						}else{
							$.getJSON(url, function(json, textStatus) {
								$.each(json, function(index, val) {
									if(index >= countItems && index < (countItems+itemLen)){
										if(val.type == "single"){
											var html = '<div class="news-block animated fadeInUp"><a href="'+val.href1+'"><figure class="cover-img"><img src="'+val.imgURL1+'" alt=""><figcaption><p>'+val.title1+'</p><span class="icon icon-arrow-right"></span></figcaption></figure></a></div>';
										}
										if(val.type == "double"){
											var html = '<div class="news-block animated fadeInUp"><a href="'+val.href1+'"><figure class="cover-img double"><img src="'+val.imgURL1+'" alt=""><figcaption><p>'+val.title1+'</p><span class="icon icon-arrow-right"></span></figcaption></figure></a><a href="'+val.href1+'"><figure class="cover-img double"><img src="'+val.imgURL2+'" alt=""><figcaption><p>'+val.title2+'</p><span class="icon icon-arrow-right"></span></figcaption></figure></a></div>';
										}
										if(index % 2 == 0){
											$('.news-section .column:first-child').append(html);
										}else{
											$('.news-section .column:last-child').append(html);
										}
									}
								});
							});
							countItems += itemLen;
						};
					break;
					case "events":
						var itemLen = 3;
						$.getJSON(url, function(json, textStatus) {
							$.each(json, function(index, val) {
								if(index >= countItems && index < (countItems+itemLen)){
									if(val.type == "half" || val.type == "big"){
										var html = '<div class="event-block '+val.type+' animated fadeInUp"><div class="event"><div class="cover-img"><img src="'+val.imgURL1+'" alt=""></div><div class="hover"><span class="name">'+val.name1+'</span><span class="day">'+val.day1+'</span><span class="month">'+val.month1+'</span><div class="adtional-info"><p>'+val.hour1+'</p><p>'+val.place1+'</p><span class="icon icon-arrow-right"></span></div></div><a href="'+val.href1+'" class="link"></a></div></div>';
									}
									if(val.type == "small"){
										var month1 = val.month1;
										var month2 = val.month2;
										var html = '<div class="event-block '+val.type+' animated fadeInUp"><div class="event"><div class="cover-img"><img src="'+val.imgURL1+'" alt=""></div><div class="hover"><span class="name">'+val.name1+'</span><span class="day">'+val.day1+'</span><span class="month">'+month1.substring(0, 3)+'</span><div class="adtional-info"><p>'+val.hour1+'</p><p>'+val.place1+'</p><span class="icon icon-arrow-right"></span></div></div><a href="'+val.href1+'" class="link"></a></div><div class="event"><div class="cover-img"><img src="'+val.imgURL2+'" alt=""></div><div class="hover"><span class="name">'+val.name2+'</span><span class="day">'+val.day2+'</span><span class="month">'+month2.substring(0, 3)+'</span><div class="adtional-info"><p>'+val.hour2+'</p><p>'+val.place2+'</p><span class="icon icon-arrow-right"></span></div></div><a href="'+val.href2+'" class="link"></a></div></div>';
									}
									$('.events-total-section').append(html);
								}
							});
							countItems += itemLen;
						});
					break;
				}
			});
		/*open-tool*/
			$('.open-tool').on('click', function(event) {
				event.preventDefault();
				$('.tool-block').hide();
				$(this).find('.tool-block').stop().slideDown(300);
			});
	});
})(jQuery);