<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">DECANOS ADJUNTOS A LA PRESIDENCIA</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNOS / DECANOS ADJUNTOS A LA PRESIDENCIA</span>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="container-small adjust">
			<div class="comite-container">
				<div class="comite">
					<a href="decanos-adjuntos-detalle.php">
						<span class="nombre">PRESIDENTE</span>
						<img class="comite-img" src="img/comite/presidente-2.jpg" alt="">
						<span class="cargo">IGNACIO LOPEZ,<br> CHOCARRO</span>
					</a>
				</div>
				<div class="comite">
					<a href="decanos-adjuntos-detalle.php">
						<span class="nombre">VICEPRESIDENTE</span>
						<img class="comite-img" src="img/comite/vicepresidente-2.jpg" alt="">
						<span class="cargo">PABLO ANTONIO, <br>BUSTAMENTE ESPARZA</span>
					</a>
				</div>
				<div class="comite">
					<a href="decanos-adjuntos-detalle.php">
						<span class="nombre">SECRETARIO</span>
						<img class="comite-img" src="img/comite/secretario-2.jpg" alt="">
						<span class="cargo">GABRIEL MARIA,<br> DE DIEGO QUEVEDO</span>
					</a>
				</div>
			</div>		
			</div>
		</div>
	</section>
<?php include("footer.php");?>