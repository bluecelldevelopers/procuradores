<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">NOTICIAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="single-news">
					<span class="news-title">El Consejo General de Procuradores de España otorga el premio Excelencia y Calidad de la Justicia a Antonio Fernández Buján y Manuel Altaba</span>
					<div class="container-half">
						<div class="image-news">
							<img src="img/content/content-01.jpg" alt="">
							<span class="destacado">DESTACADO</span>
							<span class="pie">Manuel Altaba Lavall, Juan Carlos Estévez y Antonio Fernández-Buján.</span>
						</div>	
				</div><!--
				--><div class="container-half">
						<div class="fecha-cont">
							<span class="fecha">27/07/2016</span>
						</div>
						<span class="tags">Premios, Universidad, Procuradores</span>

						<p>Antonio Fernández-Buján, Catedrático de Derecho Romano de la Universidad Autónoma de Madrid y Manuel Altaba Lavall, Senador por el Partido Popular, recibieron el Premio <span class="c_blue"> “Excelencia y en la Justicia 2015” </span> en un emotivo acto organizado por el Consejo General de Procuradores de España. El presidente de los Procuradores, Juan Carlos Estévez les entregó el galardón por su contribución a la “mejora y al funcionamiento” de la Justicia y de la Procura, así como por la protección de los derechos y la seguridad jurídica de los ciudadanos.</p>

						<p>Durante el acto de entrega, los premiados coincidieron en que los procuradores son una “pieza fundamental” en la eficacia de la administración de justicia y juegan un rol “esencial” para la modernización judicial. Destacaron la función de los procuradores como figura “garantista” que da agilidad al proceso judicial y garantiza que se cumplan las normas preestablecidas.</p>

						<p>Para Manuel Altaba, “el papel del procurador dentro del proceso judicial es vital para que todo funcione correctamente, ya que son los procuradores quienes agilizan los procesos judiciales y siguen los procedimientos desde la demanda, ocupándose de solucionar cualquier obstáculo que podría alargar el procedimiento. Por su parte, Antonio Buján, calificó a los procuradores “como aliados estratégicos en la mejora de la administración de justicia” y “cooperadores necesarios” en sus planes de modernización. </p>		
				</div>
			</div>
			<div class="noticias-sugeridas">
				<span class="sugeridas">NOTICIAS SUGERIDAS</span>
			</div>
		</div>
		</div>
	</section>
	<section>
		<div class="container-full noticias-slider">
			<div class="module-news">
				<div class="container-half c_left">
							
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="module-news">
				<div class="container-half c_left">
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
					
	</section>
	
<?php include("footer.php");?>