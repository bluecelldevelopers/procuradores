<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">CARTA PRESIDENTE</span>
					<span class="subtitle">QUIENES SOMOS</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container">
			<div class="module-news">
				<div class="single-news col-2">
					
					<P>Bienvenidos a la Página Web del Consejo General de Procuradores de los Tribunales.</p>

					<p>La modernización de la Justicia nos exige la utilización integral de las nuevas tecnologías y, con este espíritu, los Procuradores venimos colaborando, desde hace ya muchos años, con la realización de estudios, la compatibilidad de sistemas o los programas informáticos a nivel nacional.<p>

					<p>En este ánimo estamos trabajando conjuntamente con el Ministerio de Justicia, en varios proyectos telemáticos -haciendo hincapié en la seguridad, confidencialidad e integridad de todas las comunicaciones con la Administración de Justicia- y, en este punto, queremos destacar el "Proyecto LEXNET", como piloto de interconexión telemática entre la Administración de Justicia y los Procuradores.</p>

					<p>Por otro lado, esperamos poder presentaros pronto la Intranet de Procuradores, como resultado de la firma de un Acuerdo de Colaboración con la empresa Red Eléctrica de España, que unirá nuestro Consejo General, los Consejos Autonómicos y todos los Colegios de Procuradores de España entre si y, en un futuro próximo, a los despachos profesionales de todos los Procuradores.</p>

					<p>En relación con lo anterior, queremos también comunicaros la próxima publicación de la Guía de Procuradores de España, en la que aparecerán reflejados todos aquellos datos profesionales que se nos han facilitado para su publicación, de conformidad con lo establecido en la Ley Orgánica de Protección de Datos de Carácter Personal.</p>

					<p>Volviendo al ámbito de las comunicaciones y los enlaces judiciales, se encuentran avanzadas las negociaciones con los Registradores de la Propiedad, conducentes al establecimiento de una conexión telemática que permita la anotación directa de embargos en los Registros de Bienes Muebles desde nuestra Intranet.</p>		

					<p>Como resultado de todo lo anterior y, ya concretamente, en cuanto a los contenidos de la Página Web de este Consejo, queremos destacar la variedad de los mismos.</p>

					<p>Las secciones de noticias harán un seguimiento intensivo de la actualidad del sector, amén de ofrecer información sobre Jornadas, Congresos, Actividades sociales y otros eventos de interés para los profhesionales de la Procura.</p>

					<p>Una serie de recursos se interés se ofrecen también en la página Web, dirigidos a los Colegios (leyes de colegios profesionales y demás legislación aplicable), a los Procuradores (legislación, sentencias, jurisprudencia, bibliografía de interés) y al Ciudadano (Asistencia Jurídica Gratuita, Función de la Procura y otros).</p>

					<p>De especial utilidad son las bases de datos incorporadas en nuestra página Web. El directorio de Colegiados permite localizar fácilmente los datos profesionales de cualquier colegiado. Esperamos que la información sobre Colegios, Consejos autonómicos y el propio Consejo General, con sus datos de contacto y composición, permita hacer llegar vuestras sugerencias a los órganos y personas implicados en la tarea de gestionar nuestra profesión. La Ley de Demarcación y Planta figura asimismo informatizada, permitiendo consultas sobre los diferentes partidos judiciales y ofreciendo los datos de los tribunales existentes en cada demarcación.<p>

					<p>Por último, una serie de enlaces de interés a otros sitios de Internet, pertenecientes a la Administración Pública, editoriales, colegios profesionales y otros organismos permiten acceder fácilmente a otros contenidos de interés profesional con presencia en la red.</p>

					<p>En el futuro, esta página Web seguirá incorporando nuevos contenidos de interés para la profesión, además de nuevas funcionalidades y mayor interactividad, facilitando la ejecución informática de trámites ante y desde este Consejo.</p>

					<p>Esperando que el esfuerzo que hemos realizado para la elaboración de este proyecto sea de vuestro agrado y os sirva de utilidad en vuestro quehacer cotidiano, recibid mi más cordial saludo.</p>

					<div class="image-bottom">
						<div class="img">
							<img src="img/content/presidente.jpg" alt="">
						</div>
						<div class="text-img">
							<span class="nombre">ESTEVEZ FERNANDEZ-NOVOA, JUAN CARLOS</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<?php include("footer.php");?>