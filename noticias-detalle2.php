<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">NOTICIAS</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<div class="module-news">
				<div class="single-news">
					<span class="news-title">Manuel Marchena visita la nueva sede del CGPE y se entrevistó con Juan Carlos Estévez</span>
					<div class="fotogaleria-slider">
						<div class="foto-slide">
							<img src="img/content/content-01.jpg" alt="">
							<span class="pie">Manuel Marchena, presidente de la Sala de lo Penal del Tribunal Supremo y Juan Carlos Estévez, presidente del CGPE.</span>
						</div>
						<div class="foto-slide">
							<img src="img/content/content-02.jpg" alt="">
							<span class="pie">Imagen 2</span>
						</div>
						<div class="foto-slide">
							<img src="img/content/content-03.jpg" alt="">
							<span class="pie">Imagen 3</span>
						</div>
						<div class="foto-slide">
							<img src="img/content/content-04.jpg" alt="">
							<span class="pie">Imagen 4</span>
						</div>
						<div class="foto-slide">
							<img src="img/content/content-05.jpg" alt="">
							<span class="pie">Imagen 5</span>
						</div>
						<div class="foto-slide">
							<img src="img/content/content-06.jpg" alt="">
							<span class="pie">Imagen 6</span>
						</div>
					</div>
					<div class="fotogaleria-nav">
					</div>
					
					<div class="fecha-cont">
						<span class="fecha">27/07/2016</span>
					</div>
					<span class="tags">Premios, Universidad, Procuradores</span>
					<div class="text-content">
						<p>El presidente de la Sala de lo Penal del Tribunal Supremo (TS), Manuel Marchena, visito este martes la nueva sede del Consejo General de Procuradores de España, donde fue recibido por Juan Carlos Estévez, presidente del Consejo General de Procuradores de España (CGPE).</p>

						<p>El encuentro se enmarca dentro de la ronda de contactos, que el Consejo General de Procuradores de España (CGPE) está manteniendo regularmente con los máximos representantes de la Justicia para conocer de primera mano el estado y las necesidades de la misma.</p>

						<p>El Consejo General de Procuradores de España (CGPE) apuesta por el diálogo directo y fluido con todos los sectores para contribuir al establecimiento de mecanismos de mejora de nuestro sistema judicial y alcanzar una Justicia de calidad para el ciudadano.</p>

					</div>

						
				</div>
				
			</div>
			<div class="noticias-sugeridas">
				<span class="sugeridas">NOTICIAS SUGERIDAS</span>
			</div>
		</div>
	</section>
	<section>
		<div class="container-full noticias-slider">
			<div class="module-news">
				<div class="container-half c_left">
							
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="module-news">
				<div class="container-half c_left">
					<div class="item-news text">
						<div class="text-news">
							<h2>El 1º Congreso de Procuradores de los Tribunales De Castilla y León se realizará en Burgos</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div><!--
				--><div class="container-half c_right gray-back">
					<div class="item-news text">
						<div class="text-news">
							<h2>Curso técnico formativo 2016 para los empleados de los Colegios de procuradores</h2>
							<p>Los próximos 23, 24, 25 de septiembre en Burgos, se realizará el Primer Congreso de Procuradores de Castilla y León, ...</p>
							<div class="link-news">
								<a href=""><span>LEER ARTÍCULO</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
					
	</section>
	
<?php include("footer.php");?>