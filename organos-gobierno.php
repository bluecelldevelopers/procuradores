<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">COMITÉ EJECUTIVO</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNOS</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<div class="comite-container">
				<div class="comite">
					<a href="organos-gobierno-detalle.php">
						<span class="nombre">PRESIDENTE</span>
						<img class="comite-img" src="img/comite/presidente.jpg" alt="">
						<span class="cargo">ESTEVEZ FERNANDEZ-NOVOA,<br> JUAN CARLOS</span>
					</a>
				</div>
				<div class="comite">
					<a href="organos-gobierno-detalle.php">
						<span class="nombre">VICEPRESIDENTE</span>
						<img class="comite-img" src="img/comite/vicepresidente.jpg" alt="">
						<span class="cargo">SANCHEZ GARCIA, <br>JAVIER CARLOS</span>
					</a>
				</div>
				<div class="comite">
					<a href="organos-gobierno-detalle.php">
						<span class="nombre">SECRETARIO</span>
						<img class="comite-img" src="img/comite/secretario.jpg" alt="">
						<span class="cargo">VILLASANTE GARCIA,<br> JOSE MANUEL</span>
					</a>
				</div>
			</div>
			<div class="comite-container final">
				<div class="comite">
					<a href="organos-gobierno-detalle.php">
						<span class="nombre">VICESECRETARIO</span>
						<img class="comite-img" src="img/comite/vicesecretaria.jpg" alt="">
						<span class="cargo">CAPDEVILA GOMEZ,<br>MARIA DEL SOL</span>
					</a>
				</div>
				<div class="comite">
					<a href="organos-gobierno-detalle.php">
						<span class="nombre">TESORERA</span>
						<img class="comite-img" src="img/comite/tesorera.jpg" alt="">
						<span class="cargo">RUIZ-GOPEGUI GONZALEZ,<br>MARIA MERCEDES</span>
					</a>
				</div>

				<div class="comite">
					<a href="organos-gobierno-detalle.php">
						<span class="nombre">VICETESORERO</span>
						<img class="comite-img" src="img/comite/vicetesorero.jpg" alt="">
						<span class="cargo">ESTEVEZ FERNANDEZ-NOVOA, <br>JUAN CARLOS</span>
					</a>
				</div>
				
			</div>
		</div>

	</section>
	
<?php include("footer.php");?>