<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">REVISTA</span>
					<span class="subtitle">ACTUALIDAD</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			<div class="revista-container">
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista1.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 115</span>
					<span class="fecha">Enero 2016</span>
				</div>
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista2.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 114</span>
					<span class="fecha">Octubre 2015</span>
				</div>
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista3.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 113</span>
					<span class="fecha">Julio 2015</span>
				</div>
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista4.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 112</span>
					<span class="fecha">Mayo 2015</span>
				</div>
			</div>
			<div class="revista-container final">
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista5.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 111</span>
					<span class="fecha">Marzo 2015</span>
				</div>
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista6.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 110</span>
					<span class="fecha">Enero 2015</span>
				</div>
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista7.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 109</span>
					<span class="fecha">Octubre 2014</span>
				</div>
				<div class="revista">
					<div class="revista-img">
						<img src="img/content/revista8.jpg" alt="">
						<div class="over">
							<div class="descargar">
								<a href="">
									<i class="icon icon-download"></i>
									<span class="text">DESCARGAR</span>
								</a>
							</div>
							<div class="ver">
								<a href="">
									<i class="icon icon-eye"></i>
									<span class="text">VER ONLINE</span>
								</a>
							</div>
						</div>
					</div>
					<span class="numero">Número 108</span>
					<span class="fecha">Julio 2014</span>
				</div>
			</div>
		</div>

	</section>
	
<?php include("footer.php");?>