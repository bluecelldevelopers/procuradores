<?php include("header.php");?>
	<section>
		<div class="main-content">
			<div class="container">
				<div class="page-title">
					<span class="title">COMITÉ EJECUTIVO</span>
					<span class="subtitle">QUIENES SOMOS / ORGANOS DE GOBIERNOS / PRESIDENTE</span>
				</div>

			</div>	
		</div>
	</section>
	<section>
		<div class="container-small">
			
			<div class="container-half">
				<div class="detalle-cargo">
					<span>PRESIDENTE</span>
				</div>
				<div class="detalle-img">
					<img src="img/comite/presidente.jpg" alt="">
				</div>
				
			</div><!--
			--><div class="container-half">
				<div class="nombre-container">
					<span class="detalle-nombre bordeblue-bottom">JUAN CARLOS ESTEVEZ FERNANDEZ-NOVOA</span>
					<span class="bluebold">procuradores@despachoestevez.es</span>					
				</div>
				<div class="datos">
					<span class="despacho">Oficina en Madrid</span>
				</div>
				<div class="otros">
					<span class="direccion">Enrique Lareta 7, 1ºB <br>28036 - Vitoria - Álava</span>
					<div class="telefonos">
						<span class="phone"><i class="icon icon-phone"></i> +(34) 945234488</span>
						<span class="phone"><i class="icon icon-phone"></i> +(34) 945234512</span>
					</div>
					
				</div>
			</div>
			<div class="cv">
				<h5 class="resena">Reseña Curricular</h5>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra ut ex nec commodo. Donec auctor arcu quis dolor venenatis ultrices. Phasellus nec diam a turpis facilisis tincidunt dignissim nec purus.
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra ut ex nec commodo. Donec auctor arcu quis dolor venenatis ultrices. Phasellus nec diam a turpis facilisis tincidunt dignissim nec purus.
				</p>
				<div class="link-news">
					<a href="noticias.php"><span>VER MÁS</span></a>
				</div>
			</div>

		</div>

		
	</section>
<?php include("footer.php");?>